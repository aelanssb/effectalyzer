﻿using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System;

namespace Effectalyzer
{
    public class Texture
    {
        public List<byte[]> Mipmaps = new List<byte[]>(); // FIXME
        //public byte[] FullData; // kill me
        public int Width;
        public int Height;
        public int GlId;
        public GTX.SurfaceFormat Format;

        // TEMP
        public int OriginalSize;

        public int Size
        {
            get
            {
                switch (Format)
                {
                    case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC1_UNORM:
                    case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC4_UNORM:
                        return (Width * Height / 2);
                    case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC2_UNORM:
                    case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC3_UNORM:
                    case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC5_UNORM:
                        return (Width * Height);
                    default:
                        return (Width * Height);
                }
            }
        }

        public int SwizzledSize
        {
            get
            {
                int dim = Width;
                if (Height > Width)
                    dim = Height;

                switch (Format)
                {
                    case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC1_UNORM:
                    case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC4_UNORM:
                    return (dim * dim / 2);
                    case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC2_UNORM:
                    case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC3_UNORM:
                    case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC5_UNORM:
                    return (dim * dim);
                    default:
                    return (dim * dim);
                }
            }
        }

        public int getNutFormat()
        {
            switch (Format)
            {
                case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC1_UNORM:
                return 0;
                case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC2_UNORM:
                return 1;
                case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC3_UNORM:
                return 2;
                //case PixelInternalFormat.Rgba:
                //return (utype == PixelFormat.Rgba) ? 14 : 17;
                case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC4_UNORM:
                return 21;
                case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC5_UNORM:
                return 22;
                default:
                throw new NotImplementedException($"Unhandled pixel format: {Format}");
            }
        }

        public byte[] BuildNTP3Nut()
        {
            OutputBuffer o = new OutputBuffer();

            o.writeInt(0x4E545033); // "NTP3"
            o.writeShort(0x0200);
            o.writeShort(1);
            o.writeInt(0);
            o.writeInt(0);

            int size = Mipmaps[0].Length;
            int headerSize = 0x60;

            // // headerSize 0x50 seems to crash with models
            //if (texture.mipmaps.Count == 1)
            //{
            //    headerSize = 0x50;
            //}

            o.writeInt(size + headerSize);
            o.writeInt(0x00);
            o.writeInt(size);
            o.writeShort((short)headerSize);
            o.writeShort(0);
            o.writeShort(1);
            o.writeShort((short)getNutFormat());
            o.writeShort((short)Width);
            o.writeShort((short)Height);
            o.writeInt(0);
            o.writeInt(0);
            o.writeInt(0x60);
            o.writeInt(0);
            o.writeInt(0);
            o.writeInt(0);

            o.writeInt(Mipmaps[0].Length);
            o.writeInt(0);
            o.writeInt(0);
            o.writeInt(0);

            o.writeInt(0x65587400); // "eXt\0"
            o.writeInt(0x20);
            o.writeInt(0x10);
            o.writeInt(0x00);
            o.writeInt(0x47494458); // "GIDX"
            o.writeInt(0x10);
            o.writeInt(0); // texid
            o.writeInt(0);

            o.write(Mipmaps[0]);

            return o.getBytes();
        }

        public void BufferTexture()
        {
            // TODO: maybe reuse the glId and reupload.
            if (GlId != 0)
                GL.DeleteTexture(GlId);

            GlId = GL.GenTexture();

            GL.BindTexture(TextureTarget.Texture2D, GlId);

            var type = PixelInternalFormat.CompressedRedRgtc1;
            var utype = PixelFormat.Rgba;

            switch (Format)
            {
                case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC1_UNORM:
                type = PixelInternalFormat.CompressedRgbaS3tcDxt1Ext;
                break;
                case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC2_UNORM:
                type = PixelInternalFormat.CompressedRgbaS3tcDxt3Ext;
                break;
                case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC3_UNORM:
                type = PixelInternalFormat.CompressedRgbaS3tcDxt5Ext;
                break;
                case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC4_UNORM:
                type = PixelInternalFormat.CompressedRedRgtc1;
                break;
                case GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC5_UNORM:
                type = PixelInternalFormat.CompressedRgRgtc2;
                break;
                //case GTX.SurfaceFormat.RGBA:
                //type = PixelInternalFormat.Rgba;
                //utype = PixelFormat.Rgba;
                //break;
                //case GTX.SurfaceFormat.ARGB:
                //type = PixelInternalFormat.Rgba;
                //utype = PixelFormat.Bgra;
                //break;
            }

            if (type == PixelInternalFormat.Rgba)
                GL.TexImage2D(TextureTarget.Texture2D, 0, type, Width, Height, 0, utype, PixelType.UnsignedByte, Mipmaps[0]);
            else
                GL.CompressedTexImage2D(TextureTarget.Texture2D, 0, type, Width, Height, 0, Mipmaps[0].Length, Mipmaps[0]);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
        }

        public Bitmap ToBitmap()
        {
            var bmp = new Bitmap(Width, Height);
            var bmpData = bmp.LockBits(
                new Rectangle(0, 0, Width, Height),
                System.Drawing.Imaging.ImageLockMode.WriteOnly,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb
            );

            var texData = Compression.DecompressImage(this);
            Marshal.Copy(texData, 0, bmpData.Scan0, texData.Length);
            bmp.UnlockBits(bmpData);
            return bmp;
        }
    }
}
