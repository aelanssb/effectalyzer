﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Effectalyzer
{
    class ColorDiagram : GLControl
    {
        [Category("Appearance"), Description("Current value.")]
        public Vector4 Value { get; set; } = new Vector4();

        [Category("Action"), Description("Occurs when the value of the control changes.")]
        public event EventHandler ValueChanged;

        public ColorDiagram() : base()
        {
            MouseClick += ColorDiagram_MouseClick;
            Application.Idle += Application_Idle;
        }

        ~ColorDiagram()
        {
            Application.Idle -= Application_Idle;
        }

        protected void OnValueChanged (EventArgs e)
        {
            ValueChanged?.Invoke(this, e);
        }

        private void Application_Idle(object sender, EventArgs e)
        {
            if (IsDisposed || !IsIdle)
                return;

            MakeCurrent();
            GL.Clear(ClearBufferMask.ColorBufferBit);

            var ortho = Matrix4.CreateOrthographicOffCenter(
                0,
                ClientRectangle.Width,
                ClientRectangle.Height,
                0,
                0, 10000
            );

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref ortho);
            GL.Viewport(ClientRectangle);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.PushMatrix();
            GL.LoadIdentity();

            int w = ClientRectangle.Width;
            int h = ClientRectangle.Height;

            GL.ShadeModel(ShadingModel.Smooth);

            GL.BindTexture(TextureTarget.Texture2D, 0);
            GL.Begin(PrimitiveType.Quads);
            GL.Color3(Color.White);
            GL.Vertex2(0, 0);
            GL.Color4(HSVA.ToRGBA(new Vector4(Value.X, 1f, 1f, 1f)));
            GL.Vertex2(w, 0);
            GL.Color3(Color.Black);
            GL.Vertex2(w, h);
            GL.Color3(Color.Black);
            GL.Vertex2(0, h);
            GL.End();

            GL.Color3(Color.White);
            GL.PushMatrix();
            GL.Translate((Value.Y * w), (1f - Value.Z) * w, 0);
            GL.Begin(PrimitiveType.Triangles);
            GL.Vertex2(0, 0);
            GL.Vertex2(15, 0);
            GL.Vertex2(0, 15);
            GL.End();
            GL.PopMatrix();

            GL.PopMatrix();

            SwapBuffers();
        }

        private void ColorDiagram_MouseClick(object sender, MouseEventArgs e)
        {
            Console.WriteLine("fuck");
            var w = (float)ClientRectangle.Width;
            var h = (float)ClientRectangle.Height;

            //new HSV(Value.H, (int)(e.X / w), (int)((ClientRectangle.Height - e.Y) / h), Value.A);
            Value = new Vector4(Value.X, (e.X / w), ((h - e.Y) / h), Value.X);
            OnValueChanged(new EventArgs());
        }
    }
}
