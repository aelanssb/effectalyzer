﻿using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using OpenTK;

namespace Effectalyzer
{
    class HueSlider : PictureBox
    {
        [Category("Appearance"), Description("Current value.")]
        public float Value { get; set; }
        
        public HueSlider() : base()
        {
            DoubleBuffered = true;
            SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            var g = e.Graphics;
            var xs = (255f / e.ClipRectangle.Width);
            var ys = (255f / e.ClipRectangle.Height);

            for (int y = 0; y < e.ClipRectangle.Height; y++)
            {
                var hsv = new Vector4(1 - (y / (e.ClipRectangle.Y+1)), 255, 255, 255);
                //var brush = new SolidBrush(HSVA.ToColor(hsv));
                //g.FillRectangle(brush, 0, y, e.ClipRectangle.Width, 1);
            }

            var b = new SolidBrush(Color.Black);
            var yz = (int)((255 - Value) * ys);
            g.FillPie(b, new Rectangle(e.ClipRectangle.Width / 2, yz, e.ClipRectangle.Width, 16), 315, 90);
            g.FillPie(b, new Rectangle(e.ClipRectangle.Width / 2, yz, e.ClipRectangle.Width, 16), 135, 90);
        }
    }
}
