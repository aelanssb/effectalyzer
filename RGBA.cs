﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Effectalyzer
{
    public static class RGBA
    {
        public static Vector4 FromColor (Color color)
        {
            return new Vector4(color.R / 255f, color.G / 255f, color.B / 255f, color.A / 255f);
        }

        public static Color ToColor (Vector4 rgba)
        {
            return Color.FromArgb(
                (int)(Math.Max(Math.Min(rgba.W * 255, 255), 0)),
                (int)(Math.Max(Math.Min(rgba.X * 255, 255), 0)),
                (int)(Math.Max(Math.Min(rgba.Y * 255, 255), 0)),
                (int)(Math.Max(Math.Min(rgba.Z * 255, 255), 0))
            );
        }
    }
}
