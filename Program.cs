﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace Effectalyzer
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            string filename = null;
            if (args.Length > 0)
                filename = args[0];

#if DEBUG
            var name = "lucina";

            filename = $@"C:\Users\mOatles\Documents\{name}.ptcl";
            filename = $@"C:\s4explore\extract\data\fighter\{name}\effect\{name}\{name}.010d0000.ptcl";

            //filename = $@"C:\s4explore\workspace\content\patch\data\fighter\{name}\effect\{name}\{name}.010d0000.ptcl";
            ////filename = $@"C:\s43ds\romfs\data\effect\fighter\{name}\{name}.03030000.ptcl"; // 3ds
#endif
            Application.Run(new MainForm(filename));

            //using (var cpd = new ColorPickerDialog(RGBA.FromColor(Color.Bisque)))
            //{
            //    var result = cpd.ShowDialog();
            //}
        }
    }
}
