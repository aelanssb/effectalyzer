﻿using OpenTK;
using System;
using System.Drawing;

namespace Effectalyzer
{
    public static class HSVA
    {
        public static Vector4 ToRGBA(Vector4 hsv)
        {
            float hh = hsv.X;
            if (hh >= 360f)
                hh = 0f;

            hh /= 60f;
            int i = (int)hh;
            float ff = (hh - i);
            float p = hsv.Z * (1f - hsv.Y);
            float q = hsv.Z * (1f - (hsv.Y * ff));
            float t = hsv.Z * (1f - (hsv.Y * (1f - ff)));

            switch (i)
            {
                case 0:
                return new Vector4(hsv.Z, t, p, hsv.W);
                case 1:
                return new Vector4(q, hsv.Z, p, hsv.W);
                case 2:
                return new Vector4(p, hsv.Z, t, hsv.W);
                case 3:
                return new Vector4(p, q, hsv.Z, hsv.W);
                case 4:
                return new Vector4(t, p, hsv.Z, hsv.W);
                case 5:
                default:
                return new Vector4(hsv.Z, p, q, hsv.W);
            }
        }

        public static Vector4 FromRGBA(Vector4 rgba)
        {

            var hsva = new Vector4();

            var min = Math.Min(Math.Min(rgba.X, rgba.Y), rgba.Z);
            var max = Math.Max(Math.Max(rgba.X, rgba.Y), rgba.Z);

            hsva.Z = max; // v
            var delta = (max - min);
            if (delta < 0.00001f)
            {
                hsva.Y = 0;
                hsva.X = 0; // undefined, maybe nan?
                return hsva;
            }
            if (max > 0.0)
            { // NOTE: if Max is == 0, this divide would cause a crash
                hsva.Y = (delta / max);                  // s
            }
            else
            {
                // if max is 0, then r = g = b = 0              
                // s = 0, h is undefined
                hsva.Y = 0;
                hsva.X = float.NaN;                            // its now undefined
                return hsva;
            }
            if (rgba.X >= max)                           // > is bogus, just keeps compilor happy
                hsva.X = (rgba.Y - rgba.Z) / delta;        // between yellow & magenta
            else
    if (rgba.Y >= max)
                hsva.X = (2 + (rgba.Z - rgba.X) / delta);  // between cyan & yellow
            else
                hsva.X = (4 + (rgba.X - rgba.Y) / delta);  // between magenta & cyan

            hsva.X *= 60; // degrees

            while (hsva.X < 0)
                hsva.X += 360;

            return hsva;

        }

    public static Color ToColor(Vector4 hsv)
    {
            var rgba = ToRGBA(hsv) * 255;

            return Color.FromArgb(
                (int)(Math.Max(Math.Min(rgba.W, 255), 0)),
                (int)(Math.Max(Math.Min(rgba.X, 255), 0)),
                (int)(Math.Max(Math.Min(rgba.Y, 255), 0)),
                (int)(Math.Max(Math.Min(rgba.Z, 255), 0))
            );
        }
    }
}
