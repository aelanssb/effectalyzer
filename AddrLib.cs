﻿using System;
using static Effectalyzer.GTX;

namespace Effectalyzer
{
    class AddrLib
    {
        public static int GetBPP(SurfaceFormat format)
        {
            switch (format)
            {
                case SurfaceFormat.GX2_SURFACE_FORMAT_T_BC2_UNORM:
                case SurfaceFormat.GX2_SURFACE_FORMAT_T_BC3_UNORM:
                case SurfaceFormat.GX2_SURFACE_FORMAT_T_BC3_SRGB:
                case SurfaceFormat.GX2_SURFACE_FORMAT_T_BC5_UNORM:
                return 0x80;
                case SurfaceFormat.GX2_SURFACE_FORMAT_T_BC1_UNORM:
                case SurfaceFormat.GX2_SURFACE_FORMAT_T_BC1_SRGB:
                case SurfaceFormat.GX2_SURFACE_FORMAT_T_BC4_UNORM:
                case SurfaceFormat.GX2_SURFACE_FORMAT_T_BC4_SNORM:
                return 0x40;
                //case SurfaceFormat.RGBA:
                //case SurfaceFormat.ARGB:
                //return 0x20;
                default:
                throw new NotImplementedException();
            }
        }

        public static void Swizzle(Texture tex, byte[] data, int miplevel = 0, int swizzleIn = 0)
        {
            var newData = new byte[tex.Size];

            int pitch = tex.Width >> 2;
            int swizzle = (swizzleIn & 0x700) >> 8;
            int bpp = GetBPP(tex.Format);
            int blockSize = bpp / 8;

            int w = tex.Width >> miplevel;
            int h = tex.Height >> miplevel;

            //if (tex.Format != SurfaceFormat.RGBA && tex.Format != SurfaceFormat.ARGB)
            //{
            w /= 4;
            h /= 4;
            //}

            for (int i = 0; i < w * h; i++)
            {
                int pos = SurfaceAddrFromCoordMacroTiled(i % w, i / w, bpp, pitch, swizzle);
                int pos_ = i * blockSize;

                for (int k = 0; k < blockSize; k++)
                {
                    if (pos_ + k >= newData.Length || pos + k >= data.Length)
                        break;
                    newData[pos_ + k] = data[pos + k];
                }
            };

            if (miplevel < tex.Mipmaps.Count)
                tex.Mipmaps[miplevel] = newData;
            else
                tex.Mipmaps.Add(newData);
        }

        public static byte[] ReSwizzle(Texture tex, int miplevel = 0, int swizzleIn = 0)
        {
            // HACK!: This is made needlessly large until I calculate the proper surface size
            int len = (tex.SwizzledSize << 4);
            var newData = new byte[len];

            int pitch = tex.Width >> 2;
            int swizzle = (swizzleIn & 0x700) >> 8;
            int bpp = GetBPP(tex.Format);
            int blockSize = bpp / 8;

            int w = tex.Width >> miplevel;
            int h = tex.Height >> miplevel;

            //if (tex.Format != SurfaceFormat.RGBA && tex.Format != SurfaceFormat.ARGB)
            //{
            w /= 4;
            h /= 4;
            //}

            for (int i = 0; i < w * h; i++)
            {
                int pos = SurfaceAddrFromCoordMacroTiled(i % w, i / w, bpp, pitch, swizzle);
                int pos_ = i * blockSize;

                for (int k = 0; k < blockSize; k++)
                {
                    newData[pos + k] = tex.Mipmaps[miplevel][pos_ + k];
                }
            };

            Array.Resize(ref newData, (tex.Size >> (miplevel * 2)));

            //if (miplevel < tex.Mipmaps.Count)
            //    tex.Mipmaps[miplevel] = newData;
            //else
            //    tex.Mipmaps.Add(newData);

            return newData;
        }

        static int SurfaceAddrFromCoordMacroTiled(int x, int y, int bpp, int pitch, int swizzle)
        {
            int pipe = ((y ^ x) >> 3) & 1;
            int bank = (((y / 32) ^ (x >> 3)) & 1) | ((y ^ x) & 16) >> 3;
            int bankPipe = ((pipe + bank * 2) ^ swizzle) % 9;
            int macroTileBytes = (bpp * 512 + 7) >> 3;
            int macroTileOffset = (x / 32 + pitch / 32 * (y / 16)) * macroTileBytes;
            int unk = (bpp * GetPixelIndex(x, y, bpp) + macroTileOffset) / 8;

            return (bankPipe << 8) | ((bankPipe % 2) << 8) | ((unk & ~0xFF) << 3) | (unk & 0xFF);
        }

        static int GetPixelIndex(int x, int y, int bpp)
        {
            int index = (y & 6) * 8;

            if (bpp == 0x80)
                index |= ((x & 7) * 2) | (y & 1);
            else if (bpp == 0x40)
                index |= ((x & 6) * 2) | (x & 1) | ((y & 1) * 2);
            else if (bpp == 0x20)
                index |= ((x & 4) * 2) | (x & 3) | ((y & 1) * 4);

            return index;
        }
    }
}
