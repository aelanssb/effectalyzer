﻿using OpenTK;
using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Effectalyzer
{
    public partial class ColorPickerDialog : Form
    {
        const string HexColorRegex = @"^([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})?$";

        Vector4 _value = new Vector4(0, 0, 0, 1);
        public Vector4 Value
        {
            get
            {
                return _value;
            }

            set
            {
                _value = value;
                textSliderRed.Value = _value.X;
                textSliderGreen.Value = _value.Y;
                textSliderBlue.Value = _value.Z;
                textSliderAlpha.Value = _value.W;

                var color = RGBA.ToColor(value);
                var hsv = HSVA.FromRGBA(value);

                textBoxHex.Text = $"{color.R:x2}{color.G:x2}{color.B:x2}";
                LastHexValue = textBoxHex.Text;

                colorDiagram.Value = hsv;
                hueSlider.Value = (int)hsv.X;
                colorDiagram.Invalidate();
                colorPreviewNew.Color = color;
            }
        }

        string LastHexValue = "000000";
        DialogResult result = DialogResult.Cancel;

        public ColorPickerDialog(Vector4 value)
        {
            InitializeComponent();
            DoubleBuffered = true;
            Value = value;
            colorPreviewCurrent.Color = RGBA.ToColor(Value);
        }

        public new DialogResult ShowDialog()
        {
            base.ShowDialog();

            return result;
        }

        private void colorTrackBar_Scroll(object sender, EventArgs e)
        {
            Value = new Vector4(textSliderRed.Value, textSliderGreen.Value, textSliderBlue.Value, textSliderAlpha.Value);
        }

        private void textBoxHex_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var match = Regex.Match(textBoxHex.Text, HexColorRegex, RegexOptions.IgnoreCase | RegexOptions.ECMAScript);

            if (!match.Success)
                textBoxHex.Text = LastHexValue;
            else
                LastHexValue = textBoxHex.Text;
        }

        private void textBoxHex_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ValidateChildren();
                e.Handled = true;
                e.SuppressKeyPress = true;

                // Give focus back to form. It just looks nicer
                labelHex.Focus();
            }
        }

        private void buttonOkay_Click(object sender, EventArgs e)
        {
            result = DialogResult.OK;
            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            result = DialogResult.Cancel;
            Close();
        }

        private void colorDiagram_ValueChanged(object sender, EventArgs e)
        {
            Value = HSVA.ToRGBA(colorDiagram.Value);
        }
    }
}
