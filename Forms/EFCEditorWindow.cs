﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Effectalyzer
{
    public partial class EFCEditorWindow : Form
    {
        public EFCEditorWindow(PTCL ptcl, EFC efc)
        {
            InitializeComponent();
            dataGridView1.DoubleBuffered(true);

            for (int effectId = 0; effectId < efc.Effects.Count; effectId++)
            {
                var effect = efc.Effects[effectId];

                var row = new DataGridViewRow();
                row.HeaderCell.Value = effectId.ToString();

                var flagsCell = new DataGridViewTextBoxCell();
                flagsCell.Value = $"0x{effect.Unk0:X3}";
                row.Cells.Add(flagsCell);

                var particleCell = new DataGridViewComboBoxCell();
                particleCell.Items.Add("");
                foreach (var system in ptcl.Systems)
                    particleCell.Items.Add(system.Name);

                if (effect.ParticleId != -1)
                    particleCell.Value = ptcl.Systems[effect.ParticleId].Name;

                row.Cells.Add(particleCell);

                var modelCell = new DataGridViewTextBoxCell();
                if (effect.ModelNameId != -1)
                    modelCell.Value = efc.ModelNames[effect.ModelNameId];
                row.Cells.Add(modelCell);

                var meshACell = new DataGridViewTextBoxCell();
                if (effect.Unk1 != -1)
                    meshACell.Value = efc.MeshNames[effect.Unk1];
                row.Cells.Add(meshACell);

                var meshBCell = new DataGridViewTextBoxCell();
                if (effect.Unk1 != -1)
                    meshBCell.Value = efc.MeshNames[effect.Unk2];
                row.Cells.Add(meshBCell);

                dataGridView1.Rows.Add(row);
            }
        }
    }
}
