﻿using System;
using System.Windows.Forms;
using OpenTK;
using System.Drawing;
using WeifenLuo.WinFormsUI.Docking;
using OpenTK.Graphics;
using System.IO;

namespace Effectalyzer
{
    public partial class MainForm : Form
    {
        TextureListForm TextureList;
        ExplorerPanel Explorer;
        GLControl ContextControl;

        string Filename;

        public MainForm(string filename)
        {
            Filename = filename;

            InitializeComponent();
            CreateGLContext();

            Explorer = new ExplorerPanel();
            Explorer.Show(dockPanel, DockState.DockLeft);
            TextureList = new TextureListForm();
            TextureList.Show(dockPanel, DockState.DockRight);

            Explorer.NodeSelected += Explorer_NodeSelected;
            TextureList.OnTextureSelected += TextureList_OnTextureSelected;
        }

        private void TextureList_OnTextureSelected(object sender, TextureSelectedEventArgs e)
        {
            var p = new TexturePreviewPanel(e.Texture);
            p.Show(dockPanel, DockState.Document);
        }

        private void Explorer_NodeSelected(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag == null)
                return;

            var p = new EmitterPreviewPanel((PTCL.Emitter)e.Node.Tag);
            p.Show(dockPanel, DockState.Document);
        }

        void CreateGLContext()
        {
            if (ContextControl != null)
                return;

            ContextControl = new GLControl(GraphicsMode.Default, 2, 1, GraphicsContextFlags.Default);
            ContextControl.Load += new EventHandler(OnGLInit);
            ContextControl.Size = new Size(1, 1);
            Controls.Add(ContextControl);
        }

        void OnGLInit(object sender, EventArgs e)
        {
            if (Filename != null)
                LoadFile(Filename);
        }

        private void LoadFile()
        {
            var ofd = new OpenFileDialog();
            ofd.Filter = "Ptcl|*.ptcl";
            var res = ofd.ShowDialog();

            if (res == DialogResult.OK)
                LoadFile(ofd.FileName);
        }

        private void LoadFile(string filename)
        {
            Filename = filename;

            Project.Ptcl = new PTCL(filename);
            Project.Efc = new EFC(Path.ChangeExtension(filename, "efc"));
            Text = $"{Project.Ptcl.fileName} - Effectalyzer";

            Reset();
        }

        void Reset()
        {
            Explorer.Reset();
            TextureList.Reset();

            foreach (var pane in dockPanel.Panes)
            {
                if (pane.Appearance == DockPane.AppearanceStyle.Document)
                    pane.CloseActiveContent();
            }
        }

        private void MainForm_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }

        private void MainForm_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            LoadFile(files[0]);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadFile();
        }

        private void eFCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var efcWindow = new EFCEditorWindow(Project.Ptcl, Project.Efc))
                efcWindow.ShowDialog();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var s = new FileStream(Project.Ptcl.OriginalFilename, FileMode.Create))
            {
                var d = Project.Ptcl.Rebuild();
                s.Write(d, 0, d.Length);
            }
        }
    }
}
