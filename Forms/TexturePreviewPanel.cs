﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Drawing;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace Effectalyzer
{
    public partial class TexturePreviewPanel : DockContent
    {
        Texture Texture;

        public TexturePreviewPanel(Texture tex)
        {
            InitializeComponent();

            Texture = tex;
            TabText = "Texture";
        }

        ~TexturePreviewPanel()
        {
            Application.Idle -= Application_Idle;
        }

        private void glControl_Load(object sender, EventArgs e)
        {
            GL.ClearColor(Color.Black);
            GL.Enable(EnableCap.Texture2D);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            Application.Idle += Application_Idle;
        }

        private void Application_Idle(object sender, EventArgs e)
        {
            Render();
        }

        void Render()
        {
            if (glControl.IsDisposed || !glControl.IsIdle)
                return;

            glControl.MakeCurrent();
            GL.Clear(ClearBufferMask.ColorBufferBit);

            var ortho = Matrix4.CreateOrthographicOffCenter(
                0,
                1,
                1,
                0,
                0, 10
            );

            GL.MatrixMode(MatrixMode.Projection);
            GL.PushMatrix();
            GL.LoadMatrix(ref ortho);
            GL.Viewport(glControl.ClientRectangle);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.PushMatrix();
            GL.LoadIdentity();

            float w = 1.0f;
            float h = 1.0f;

            GL.BindTexture(TextureTarget.Texture2D, Texture.GlId);
            GL.Begin(PrimitiveType.Quads);
            GL.TexCoord2(0, 0);
            GL.Vertex2(0, 0);
            GL.TexCoord2(1, 0);
            GL.Vertex2(w, 0);
            GL.TexCoord2(1, 1);
            GL.Vertex2(w, h);
            GL.TexCoord2(0, 1);
            GL.Vertex2(0, h);
            GL.End();

            GL.PopMatrix();
            GL.MatrixMode(MatrixMode.Projection);
            GL.PopMatrix();
            GL.MatrixMode(MatrixMode.Modelview);
            GL.PopAttrib();

            glControl.SwapBuffers();
        }
    }
}
