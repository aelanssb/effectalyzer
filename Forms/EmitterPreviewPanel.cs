﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Drawing;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace Effectalyzer
{
    public partial class EmitterPreviewPanel : DockContent
    {
        PTCL.Emitter Emitter;

        public EmitterPreviewPanel(PTCL.Emitter emitter)
        {
            InitializeComponent();

            Emitter = emitter;
            TabText = $"{Emitter.Parent.Name}.{Emitter.Name}";

            dataGridView1.DoubleBuffered(true);

            for (int i = 0, o = 0; i < emitter.Data.Count; i++)
            {
                var row = new DataGridViewRow();
                row.Tag = i;

                var data = emitter.Data[i];
                var field = PTCL.EmitterFields[i];

                var offsetCell = new DataGridViewTextBoxCell();
                offsetCell.Value = $"0x{o:X3}";
                row.Cells.Add(offsetCell);

                var nameCell = new DataGridViewTextBoxCell();
                if (field != null)
                    nameCell.Value = field.Name;
                row.Cells.Add(nameCell);

                DataGridViewCell valueCell = new DataGridViewTextBoxCell();

                if (field != null)
                {
                    if (field.Type == "float")
                    {
                        valueCell.Value = ((float)data).ToString();
                        o += 4;
                    }
                    else if (field.Type == "color")
                    {
                        valueCell = new DataGridViewColorCell();
                        valueCell.Value = (Vector4)data;
                        ((DataGridViewColorCell)valueCell).SetStyleColors();

                        o += 16;
                    }
                    else if (field.Type == "uint")
                    {
                        valueCell.Value = $"0x{(int)data:X8}";
                        o += 4;
                    }
                    else
                    {
                        valueCell.Value = ((int)data).ToString();
                        o += 4;
                    }
                }
                else
                {
                    valueCell.Value = $"0x{((int)data):X8}";
                    o += 4;
                }
                row.Cells.Add(valueCell);

                dataGridView1.Rows.Add(row);
            }
        }

        ~EmitterPreviewPanel()
        {
            Application.Idle -= Application_Idle;
        }

        private void glControl_Load(object sender, EventArgs e)
        {
            GL.ClearColor(Color.Black);
            GL.Enable(EnableCap.Texture2D);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            Application.Idle += Application_Idle;
        }

        private void Application_Idle(object sender, EventArgs e)
        {
            if (glControl.IsDisposed || !glControl.IsIdle)
                return;

            glControl.MakeCurrent();
            GL.Clear(ClearBufferMask.ColorBufferBit);

            var ortho = Matrix4.CreateOrthographicOffCenter(
                0,
                1,
                1,
                0,
                0, 10000
            );

            GL.MatrixMode(MatrixMode.Projection);
            GL.PushMatrix();
            GL.LoadMatrix(ref ortho);
            GL.Viewport(glControl.ClientRectangle);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.PushMatrix();
            GL.LoadIdentity();

            float w = 0.5f;
            float h = 0.5f;

            Texture tex = null;
            for (int shtId = 0; shtId < 3; shtId++)
            {
                if (Emitter.TextureId[shtId] == -1)
                    continue;

                tex = Project.Ptcl.Textures[Emitter.TextureId[shtId]];

                float x = (shtId % 2) * w;
                float y = (shtId / 2) * h;

                GL.PushMatrix();
                GL.Translate(x, y, 0);
                GL.BindTexture(TextureTarget.Texture2D, tex.GlId);
                GL.Begin(PrimitiveType.Quads);
                GL.TexCoord2(0, 0);
                GL.Vertex2(0, 0);
                GL.TexCoord2(1, 0);
                GL.Vertex2(w, 0);
                GL.TexCoord2(1, 1);
                GL.Vertex2(w, h);
                GL.TexCoord2(0, 1);
                GL.Vertex2(0, h);
                GL.End();
                GL.PopMatrix();
            }

            GL.PopMatrix();
            GL.MatrixMode(MatrixMode.Projection);
            GL.PopMatrix();
            GL.MatrixMode(MatrixMode.Modelview);
            GL.PopAttrib();

            glControl.SwapBuffers();
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
                return;

            var row = dataGridView1.Rows[e.RowIndex];
            var cell = row.Cells[e.ColumnIndex];
            var i = (int)row.Tag;
            var o = i * 4;

            if (PTCL.EmitterFields.ContainsKey(o))
            {
                switch (PTCL.EmitterFields[o].Type)
                {
                    case "uint":
                    Emitter.Data[i] = new System.ComponentModel.UInt32Converter().ConvertFromString((string)cell.Value);
                    break;
                    case "int":
                    Emitter.Data[i] = new System.ComponentModel.Int32Converter().ConvertFromString((string)cell.Value);
                    break;
                    case "float":
                    Emitter.Data[i] = new System.ComponentModel.SingleConverter().ConvertFromString((string)cell.Value);
                    break;
                    case "color":
                    Emitter.Data[i] = cell.Value;
                    break;
                    default:
                    Emitter.Data[i] = new System.ComponentModel.Int32Converter().ConvertFromString((string)cell.Value);
                    break;
                }
            }
            else
            {
                Emitter.Data[i] = (int)new System.ComponentModel.UInt32Converter().ConvertFromString((string)cell.Value);

            }
        }
    }
}
