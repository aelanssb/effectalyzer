﻿namespace Effectalyzer
{
    partial class ColorPickerDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ColorPickerDialog));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelHex = new System.Windows.Forms.Label();
            this.textBoxHex = new System.Windows.Forms.TextBox();
            this.buttonOkay = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.textSliderRed = new Effectalyzer.TextSlider();
            this.textSliderGreen = new Effectalyzer.TextSlider();
            this.textSliderBlue = new Effectalyzer.TextSlider();
            this.textSliderAlpha = new Effectalyzer.TextSlider();
            this.colorPreviewCurrent = new Lumenati.ColorPreview();
            this.colorPreviewNew = new Lumenati.ColorPreview();
            this.hueSlider = new Effectalyzer.HueSlider();
            this.colorDiagram = new Effectalyzer.ColorDiagram();
            ((System.ComponentModel.ISupportInitialize)(this.hueSlider)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(423, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "R:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(423, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "G:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(424, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "B:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(424, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "A:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelHex
            // 
            this.labelHex.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelHex.AutoSize = true;
            this.labelHex.BackColor = System.Drawing.Color.Transparent;
            this.labelHex.Location = new System.Drawing.Point(409, 209);
            this.labelHex.Name = "labelHex";
            this.labelHex.Size = new System.Drawing.Size(16, 17);
            this.labelHex.TabIndex = 8;
            this.labelHex.Text = "#";
            this.labelHex.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxHex
            // 
            this.textBoxHex.Location = new System.Drawing.Point(427, 204);
            this.textBoxHex.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxHex.Name = "textBoxHex";
            this.textBoxHex.Size = new System.Drawing.Size(77, 22);
            this.textBoxHex.TabIndex = 9;
            this.textBoxHex.Text = "000000";
            // 
            // buttonOkay
            // 
            this.buttonOkay.Location = new System.Drawing.Point(416, 266);
            this.buttonOkay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonOkay.Name = "buttonOkay";
            this.buttonOkay.Size = new System.Drawing.Size(88, 27);
            this.buttonOkay.TabIndex = 16;
            this.buttonOkay.Text = "OK";
            this.buttonOkay.UseVisualStyleBackColor = true;
            this.buttonOkay.Click += new System.EventHandler(this.buttonOkay_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(416, 300);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(88, 27);
            this.buttonCancel.TabIndex = 17;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // textSliderRed
            // 
            this.textSliderRed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textSliderRed.AutoSize = true;
            this.textSliderRed.BackColor = System.Drawing.Color.Transparent;
            this.textSliderRed.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.textSliderRed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline);
            this.textSliderRed.ForeColor = System.Drawing.Color.Blue;
            this.textSliderRed.Location = new System.Drawing.Point(445, 121);
            this.textSliderRed.MaximumValue = 1F;
            this.textSliderRed.MinimumValue = 0F;
            this.textSliderRed.Mode = Effectalyzer.TextSliderMode.Slider;
            this.textSliderRed.Name = "textSliderRed";
            this.textSliderRed.Size = new System.Drawing.Size(16, 17);
            this.textSliderRed.Step = 0.002F;
            this.textSliderRed.TabIndex = 10;
            this.textSliderRed.Text = "0";
            this.textSliderRed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.textSliderRed.Value = 0F;
            this.textSliderRed.ValueChanged += new System.EventHandler(this.colorTrackBar_Scroll);
            // 
            // textSliderGreen
            // 
            this.textSliderGreen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textSliderGreen.AutoSize = true;
            this.textSliderGreen.BackColor = System.Drawing.Color.Transparent;
            this.textSliderGreen.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.textSliderGreen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline);
            this.textSliderGreen.ForeColor = System.Drawing.Color.Blue;
            this.textSliderGreen.Location = new System.Drawing.Point(445, 137);
            this.textSliderGreen.MaximumValue = 1F;
            this.textSliderGreen.MinimumValue = 0F;
            this.textSliderGreen.Mode = Effectalyzer.TextSliderMode.Slider;
            this.textSliderGreen.Name = "textSliderGreen";
            this.textSliderGreen.Size = new System.Drawing.Size(16, 17);
            this.textSliderGreen.Step = 0.002F;
            this.textSliderGreen.TabIndex = 11;
            this.textSliderGreen.Text = "0";
            this.textSliderGreen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.textSliderGreen.Value = 0F;
            this.textSliderGreen.ValueChanged += new System.EventHandler(this.colorTrackBar_Scroll);
            // 
            // textSliderBlue
            // 
            this.textSliderBlue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textSliderBlue.AutoSize = true;
            this.textSliderBlue.BackColor = System.Drawing.Color.Transparent;
            this.textSliderBlue.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.textSliderBlue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline);
            this.textSliderBlue.ForeColor = System.Drawing.Color.Blue;
            this.textSliderBlue.Location = new System.Drawing.Point(445, 153);
            this.textSliderBlue.MaximumValue = 1F;
            this.textSliderBlue.MinimumValue = 0F;
            this.textSliderBlue.Mode = Effectalyzer.TextSliderMode.Slider;
            this.textSliderBlue.Name = "textSliderBlue";
            this.textSliderBlue.Size = new System.Drawing.Size(16, 17);
            this.textSliderBlue.Step = 0.002F;
            this.textSliderBlue.TabIndex = 12;
            this.textSliderBlue.Text = "0";
            this.textSliderBlue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.textSliderBlue.Value = 0F;
            this.textSliderBlue.ValueChanged += new System.EventHandler(this.colorTrackBar_Scroll);
            // 
            // textSliderAlpha
            // 
            this.textSliderAlpha.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textSliderAlpha.AutoSize = true;
            this.textSliderAlpha.BackColor = System.Drawing.Color.Transparent;
            this.textSliderAlpha.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.textSliderAlpha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline);
            this.textSliderAlpha.ForeColor = System.Drawing.Color.Blue;
            this.textSliderAlpha.Location = new System.Drawing.Point(445, 169);
            this.textSliderAlpha.MaximumValue = 1F;
            this.textSliderAlpha.MinimumValue = 0F;
            this.textSliderAlpha.Mode = Effectalyzer.TextSliderMode.Slider;
            this.textSliderAlpha.Name = "textSliderAlpha";
            this.textSliderAlpha.Size = new System.Drawing.Size(16, 17);
            this.textSliderAlpha.Step = 0.002F;
            this.textSliderAlpha.TabIndex = 13;
            this.textSliderAlpha.Text = "0";
            this.textSliderAlpha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.textSliderAlpha.Value = 0F;
            this.textSliderAlpha.ValueChanged += new System.EventHandler(this.colorTrackBar_Scroll);
            // 
            // colorPreviewCurrent
            // 
            this.colorPreviewCurrent.Color = System.Drawing.Color.Black;
            this.colorPreviewCurrent.Location = new System.Drawing.Point(427, 66);
            this.colorPreviewCurrent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.colorPreviewCurrent.Name = "colorPreviewCurrent";
            this.colorPreviewCurrent.Size = new System.Drawing.Size(77, 41);
            this.colorPreviewCurrent.TabIndex = 19;
            // 
            // colorPreviewNew
            // 
            this.colorPreviewNew.Color = System.Drawing.Color.Black;
            this.colorPreviewNew.Location = new System.Drawing.Point(427, 26);
            this.colorPreviewNew.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.colorPreviewNew.Name = "colorPreviewNew";
            this.colorPreviewNew.Size = new System.Drawing.Size(77, 41);
            this.colorPreviewNew.TabIndex = 18;
            // 
            // hueSlider
            // 
            this.hueSlider.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.hueSlider.Location = new System.Drawing.Point(365, 15);
            this.hueSlider.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.hueSlider.Name = "hueSlider";
            this.hueSlider.Size = new System.Drawing.Size(35, 314);
            this.hueSlider.TabIndex = 15;
            this.hueSlider.TabStop = false;
            this.hueSlider.Value = 0F;
            // 
            // colorDiagram
            // 
            this.colorDiagram.BackColor = System.Drawing.Color.Black;
            this.colorDiagram.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.colorDiagram.Location = new System.Drawing.Point(16, 15);
            this.colorDiagram.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.colorDiagram.Name = "colorDiagram";
            this.colorDiagram.Size = new System.Drawing.Size(340, 314);
            this.colorDiagram.TabIndex = 14;
            this.colorDiagram.TabStop = false;
            this.colorDiagram.Value = ((OpenTK.Vector4)(resources.GetObject("colorDiagram.Value")));
            this.colorDiagram.VSync = false;
            this.colorDiagram.ValueChanged += new System.EventHandler(this.colorDiagram_ValueChanged);
            // 
            // ColorPickerDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 340);
            this.Controls.Add(this.textBoxHex);
            this.Controls.Add(this.textSliderRed);
            this.Controls.Add(this.textSliderGreen);
            this.Controls.Add(this.textSliderBlue);
            this.Controls.Add(this.textSliderAlpha);
            this.Controls.Add(this.colorPreviewCurrent);
            this.Controls.Add(this.colorPreviewNew);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.labelHex);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.hueSlider);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.colorDiagram);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonOkay);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ColorPickerDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Color Picker";
            ((System.ComponentModel.ISupportInitialize)(this.hueSlider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private TextSlider textSliderAlpha;
        private TextSlider textSliderBlue;
        private TextSlider textSliderGreen;
        private TextSlider textSliderRed;
        private System.Windows.Forms.Label labelHex;
        private ColorDiagram colorDiagram;
        private Effectalyzer.HueSlider hueSlider;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOkay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxHex;
        private Lumenati.ColorPreview colorPreviewNew;
        private Lumenati.ColorPreview colorPreviewCurrent;
    }
}