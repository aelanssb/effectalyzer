﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace Effectalyzer
{
    public partial class TextureListForm : DockContent
    {
        public List<Texture> Textures = new List<Texture>();
        public event EventHandler<TextureSelectedEventArgs> OnTextureSelected;

        public TextureListForm() : base()
        {
            InitializeComponent();

            listViewTextures.LargeImageList = new ImageList();
            listViewTextures.LargeImageList.ImageSize = new Size(48, 48);
        }

        public void Add(Texture tex)
        {
            var id = listViewTextures.Items.Count;
            var item = new ListViewItem($"0x{id:X2}: {tex.Width}x{tex.Height}"); // , {tex.Mipmaps.Count} mips
            item.ImageIndex = id;
            
            listViewTextures.LargeImageList.Images.Add(tex.ToBitmap());
            listViewTextures.Items.Add(item);
            Textures.Add(tex);
        }

        public void Reset()
        {
            listViewTextures.Items.Clear();
            listViewTextures.LargeImageList.Images.Clear();
            Textures.Clear();

            foreach (var tex in Project.Ptcl.Textures)
                Add(tex);
        }

        private void listViewTextures_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var senderList = (ListView)sender;
            var clickedItem = senderList.HitTest(e.Location).Item;
            if (clickedItem != null)
            {
                var tex = Textures[clickedItem.ImageIndex];
                OnTextureSelected?.Invoke(this, new TextureSelectedEventArgs(tex));
            }
        }

        private void listViewTextures_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var senderList = (ListView)sender;
                var clickedItem = senderList.HitTest(e.Location).Item;
                if (clickedItem != null)
                {
                    var tex = Textures[clickedItem.ImageIndex];
                    contextMenuStrip1.Show(listViewTextures, e.Location);
                }
            }
        }

        private void replaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Filter = "GTX Texture|*.gtx";

            if (ofd.ShowDialog() != DialogResult.OK)
                return;

            var gtx = new GTX(ofd.FileName);
            var item = listViewTextures.SelectedItems[0];
            var tex = Textures[item.ImageIndex];

            if (gtx.Surface.format != tex.Format)
            {
                var dialogResult = MessageBox.Show(
                    $"{ofd.FileName}'s format \"{gtx.Surface.format}\" does not match original \"{tex.Format}\"\n\nThis will probably shit the bed. Try anyway?",
                    "Format Mismatch",
                    MessageBoxButtons.YesNo
                );

                if (dialogResult == DialogResult.No)
                    return;
            }

            if (gtx.Surface.width != tex.Width || gtx.Surface.height != tex.Height)
            {
                var dialogResult = MessageBox.Show(
                    $"{ofd.FileName}'s dimensions {gtx.Surface.width}x{gtx.Surface.height} does not match original {tex.Width}x{tex.Height}\n\nThis will probably shit the bed. Try anyway?",
                    "Dimension Mismatch",
                    MessageBoxButtons.YesNo
                );

                if (dialogResult == DialogResult.No)
                    return;
            }

            //Array.Copy(gtx.Data, tex.FullData, gtx.Data.Length);
            AddrLib.Swizzle(tex, gtx.Data, 0, (int)gtx.Surface.swizzle);
            listViewTextures.LargeImageList.Images[item.ImageIndex] = tex.ToBitmap();
            tex.BufferTexture();
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var sfd = new SaveFileDialog())
            {
                sfd.Filter = "PS3 Nut|*.nut";

                if (sfd.ShowDialog() != DialogResult.OK)
                    return;

                var item = listViewTextures.SelectedItems[0];
                var data = Textures[item.ImageIndex].BuildNTP3Nut();

                using (var stream = new FileStream(sfd.FileName, FileMode.Create))
                {
                    stream.Write(data, 0, data.Length);
                }
            }
        }
    }

    public class TextureSelectedEventArgs : EventArgs
    {
        public Texture Texture;

        public TextureSelectedEventArgs(Texture tex) : base()
        {
            Texture = tex;
        }
    }
}
