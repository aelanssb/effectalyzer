﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace Effectalyzer
{
    public partial class ExplorerPanel : DockContent
    {
        public event EventHandler<TreeNodeMouseClickEventArgs> NodeSelected;

        public ExplorerPanel()
        {
            InitializeComponent();

            // force selection on right-click
            treeView1.NodeMouseClick += (sender, args) => treeView1.SelectedNode = args.Node;
            treeView1.NodeMouseDoubleClick += (sender, args) => NodeSelected?.Invoke(sender, args);
        }

        public void Reset()
        {
            treeView1.Nodes.Clear();

            foreach (var system in Project.Ptcl.Systems)
            {
                var treeNode = new TreeNode(system.Name);

                foreach (var emitter in system.Emitters)
                {
                    var newNode = new TreeNode(emitter.Name);
                    newNode.Tag = emitter;
                    treeNode.Nodes.Add(newNode);
                }

                treeView1.Nodes.Add(treeNode);
            }
        }
    }
}
