﻿namespace Effectalyzer
{
    public class GTX
    {
        public enum SurfaceFormat : uint
        {
            GX2_SURFACE_FORMAT_INVALID = 0x00,
            GX2_SURFACE_FORMAT_FIRST = 0x01,
            GX2_SURFACE_FORMAT_TC_R8_UNORM = 0x01,
            GX2_SURFACE_FORMAT_T_R4_G4_UNORM = 0x02,
            GX2_SURFACE_FORMAT_TCD_R16_UNORM = 0x05,
            GX2_SURFACE_FORMAT_TC_R8_G8_UNORM = 0x07,
            GX2_SURFACE_FORMAT_TCS_R5_G6_B5_UNORM = 0x08,
            GX2_SURFACE_FORMAT_TC_R5_G5_B5_A1_UNORM = 0x0A,
            GX2_SURFACE_FORMAT_TC_R4_G4_B4_A4_UNORM = 0x0B,
            GX2_SURFACE_FORMAT_TC_A1_B5_G5_R5_UNORM = 0x0C,
            GX2_SURFACE_FORMAT_TC_R16_G16_UNORM = 0x0F,
            GX2_SURFACE_FORMAT_D_D24_S8_UNORM = 0x11,
            GX2_SURFACE_FORMAT_T_R24_UNORM_X8 = 0x11,
            GX2_SURFACE_FORMAT_TCS_R10_G10_B10_A2_UNORM = 0x19,
            GX2_SURFACE_FORMAT_TCS_R8_G8_B8_A8_UNORM = 0x1A,
            GX2_SURFACE_FORMAT_TCS_A2_B10_G10_R10_UNORM = 0x1B,
            GX2_SURFACE_FORMAT_TC_R16_G16_B16_A16_UNORM = 0x1F,
            GX2_SURFACE_FORMAT_T_BC1_UNORM = 0x31,
            GX2_SURFACE_FORMAT_T_BC2_UNORM = 0x32,
            GX2_SURFACE_FORMAT_T_BC3_UNORM = 0x33,
            GX2_SURFACE_FORMAT_T_BC4_UNORM = 0x34,
            GX2_SURFACE_FORMAT_T_BC5_UNORM = 0x35,
            GX2_SURFACE_FORMAT_T_NV12_UNORM = 0x81,
            GX2_SURFACE_FORMAT_TC_R8_UINT = 0x101,
            GX2_SURFACE_FORMAT_TC_R16_UINT = 0x105,
            GX2_SURFACE_FORMAT_TC_R8_G8_UINT = 0x107,
            GX2_SURFACE_FORMAT_TC_R32_UINT = 0x10D,
            GX2_SURFACE_FORMAT_TC_R16_G16_UINT = 0x10F,
            GX2_SURFACE_FORMAT_T_X24_G8_UINT = 0x111,
            GX2_SURFACE_FORMAT_TC_R10_G10_B10_A2_UINT = 0x119,
            GX2_SURFACE_FORMAT_TC_R8_G8_B8_A8_UINT = 0x11A,
            GX2_SURFACE_FORMAT_TC_A2_B10_G10_R10_UINT = 0x11B,
            GX2_SURFACE_FORMAT_T_X32_G8_UINT_X24 = 0x11C,
            GX2_SURFACE_FORMAT_TC_R32_G32_UINT = 0x11D,
            GX2_SURFACE_FORMAT_TC_R16_G16_B16_A16_UINT = 0x11F,
            GX2_SURFACE_FORMAT_TC_R32_G32_B32_A32_UINT = 0x122,
            GX2_SURFACE_FORMAT_TC_R8_SNORM = 0x201,
            GX2_SURFACE_FORMAT_TC_R16_SNORM = 0x205,
            GX2_SURFACE_FORMAT_TC_R8_G8_SNORM = 0x207,
            GX2_SURFACE_FORMAT_TC_R16_G16_SNORM = 0x20F,
            GX2_SURFACE_FORMAT_T_R10_G10_B10_A2_SNORM = 0x219,
            GX2_SURFACE_FORMAT_TC_R10_G10_B10_A2_SNORM = 0x219,
            GX2_SURFACE_FORMAT_TC_R8_G8_B8_A8_SNORM = 0x21A,
            GX2_SURFACE_FORMAT_TC_R16_G16_B16_A16_SNORM = 0x21F,
            GX2_SURFACE_FORMAT_T_BC4_SNORM = 0x234,
            GX2_SURFACE_FORMAT_T_BC5_SNORM = 0x235,
            GX2_SURFACE_FORMAT_TC_R8_SINT = 0x301,
            GX2_SURFACE_FORMAT_TC_R16_SINT = 0x305,
            GX2_SURFACE_FORMAT_TC_R8_G8_SINT = 0x307,
            GX2_SURFACE_FORMAT_TC_R32_SINT = 0x30D,
            GX2_SURFACE_FORMAT_TC_R16_G16_SINT = 0x30F,
            GX2_SURFACE_FORMAT_TC_R10_G10_B10_A2_SINT = 0x319,
            GX2_SURFACE_FORMAT_TC_R8_G8_B8_A8_SINT = 0x31A,
            GX2_SURFACE_FORMAT_TC_R32_G32_SINT = 0x31D,
            GX2_SURFACE_FORMAT_TC_R16_G16_B16_A16_SINT = 0x31F,
            GX2_SURFACE_FORMAT_TC_R32_G32_B32_A32_SINT = 0x322,
            GX2_SURFACE_FORMAT_TCS_R8_G8_B8_A8_SRGB = 0x41A,
            GX2_SURFACE_FORMAT_T_BC1_SRGB = 0x431,
            GX2_SURFACE_FORMAT_T_BC2_SRGB = 0x432,
            GX2_SURFACE_FORMAT_T_BC3_SRGB = 0x433,
            GX2_SURFACE_FORMAT_TC_R16_FLOAT = 0x806,
            GX2_SURFACE_FORMAT_TCD_R32_FLOAT = 0x80E,
            GX2_SURFACE_FORMAT_TC_R16_G16_FLOAT = 0x810,
            GX2_SURFACE_FORMAT_D_D24_S8_FLOAT = 0x811,
            GX2_SURFACE_FORMAT_TC_R11_G11_B10_FLOAT = 0x816,
            GX2_SURFACE_FORMAT_D_D32_FLOAT_S8_UINT_X24 = 0x81C,
            GX2_SURFACE_FORMAT_T_R32_FLOAT_X8_X24 = 0x81C,
            GX2_SURFACE_FORMAT_TC_R32_G32_FLOAT = 0x81E,
            GX2_SURFACE_FORMAT_TC_R16_G16_B16_A16_FLOAT = 0x820,
            GX2_SURFACE_FORMAT_TC_R32_G32_B32_A32_FLOAT = 0x823,
            GX2_SURFACE_FORMAT_LAST = 0x83F
        }

        public struct GFDHeader
        {
            public uint magic; // Gfx2
            public uint size;
            public uint majorVersion;
            public uint minorVersion;
            public uint gpuVersion;
            public uint alignMode;
            public uint reserved1;
            public uint reserved2;
        }

        enum GFDBlockType : uint
        {
            Header = 0,    
            End = 1,    
            PAD = 2,    
            GX2_VSH_HEADER = 3,    
            GX2_VSH_PROGRAM = 5,    
            GX2_PSH_HEADER = 6,    
            GX2_PSH_PROGRAM = 7,    
            GX2_GSH_HEADER = 8,    
            GX2_GSH_PROGRAM = 9,    
            GX2_GSH_COPY_PROGRAM = 10,   
            TexHeader = 0x0B,   
            TexImage = 0x0C,   
            GX2_TEX_MIP_IMAGE = 13,   
            GX2_CSH_HEADER = 14,   
            GX2_CSH_PROGRAM = 15,   
            USER = 16,
        }

        struct GFDBlockHeader
        {
            public uint magic; // BLK{
            public uint size;
            public uint majorVersion;
            public uint minorVersion;
            public GFDBlockType Type;
            public uint dataSize;
            public uint id;
            public uint typeId;
        }

        public struct GX2Surface
        {
            public uint dim;
            public uint width;
            public uint height;
            public uint depth;
            public uint numMips;
            public SurfaceFormat format;
            public uint aa;
            public uint use;
            public uint imageSize;
            public uint imagePtr;
            public uint mipSize;
            public uint mipPtr;
            public uint tileMode;
            public uint swizzle;
            public uint alignment;
            public uint pitch;
        }

        struct GX2Texture
        {
            public GX2Surface surface;
            public uint viewFirstMip;
            public uint viewNumMips;
            public uint viewFirstSlice;
            public uint viewNumSlices;
            public uint compSel;

            public uint _regs0;
            public uint _regs1;
            public uint _regs2;
            public uint _regs3;
            public uint _regs4;
        }

        public byte[] Data;
        public GX2Surface Surface = new GX2Surface();

        public GFDHeader header;

        public GTX(string filename)
        {
            var file = new InputBuffer(filename);

            header = new GFDHeader();
            header.magic = file.readUint();
            header.size = file.readUint();
            header.majorVersion = file.readUint();
            header.minorVersion = file.readUint();
            header.gpuVersion = file.readUint();
            header.alignMode = file.readUint();
            header.reserved1 = file.readUint();
            header.reserved2 = file.readUint();

            while (true)
            {
                var blockHeader = new GFDBlockHeader();
                blockHeader.magic = file.readUint(); // BLK{
                blockHeader.size = file.readUint();
                blockHeader.majorVersion = file.readUint();
                blockHeader.minorVersion = file.readUint();
                blockHeader.Type = (GFDBlockType)file.readUint();
                blockHeader.dataSize = file.readUint();
                blockHeader.id = file.readUint();
                blockHeader.typeId = file.readUint();

                if (blockHeader.Type == GFDBlockType.TexHeader)
                {
                    Surface.dim = file.readUint();
                    Surface.width = file.readUint();
                    Surface.height = file.readUint();
                    Surface.depth = file.readUint();
                    Surface.numMips = file.readUint();
                    Surface.format = (SurfaceFormat)file.readUint();
                    Surface.aa = file.readUint();
                    Surface.use = file.readUint();
                    Surface.imageSize = file.readUint();
                    Surface.imagePtr = file.readUint();
                    Surface.mipSize = file.readUint();
                    Surface.mipPtr = file.readUint();
                    Surface.tileMode = file.readUint();
                    Surface.swizzle = file.readUint();
                    Surface.alignment = file.readUint();
                    Surface.pitch = file.readUint();

                    file.Ptr += (blockHeader.dataSize - 0x40);
                }
                else if (blockHeader.Type == GFDBlockType.TexImage)
                {
                    Data = file.read((int)blockHeader.dataSize);
                    break;
                }
                else
                {
                    file.Ptr += blockHeader.dataSize;
                }
            }
        }
    }
}
