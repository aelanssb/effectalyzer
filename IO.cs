﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Effectalyzer
{
    public enum Endian
    {
        Big,
        Little
    }

    public class InputBuffer
    {
        public byte[] Data;
        public uint Ptr;
        public Endian Endianness = Endian.Big;

        public InputBuffer(string filename, Endian endian = Endian.Big)
        {
            Endianness = endian;
            Data = File.ReadAllBytes(filename);
        }

        public byte[] read(int size)
        {
            if (size + Ptr > Data.Length)
                throw new IndexOutOfRangeException();

            var o = new byte[size];

            Array.Copy(Data, Ptr, o, 0, size);

            Ptr += (uint)size;

            return o;
        }

        public byte[] read(int offset, int size)
        {
            if (size + offset > Data.Length)
                throw new IndexOutOfRangeException();

            var o = new byte[size];
            Array.Copy(Data, offset, o, 0, size);

            return o;
        }

        public string readSizedString()
        {
            uint length = (uint)readInt();

            byte[] d = new byte[length];
            Array.Copy(Data, Ptr, d, 0, length);

            Ptr += length;

            return Encoding.UTF8.GetString(d);
        }

        public string readCString()
        {
            var str = readString();
            Ptr++;
            return str;
        }

        public string readString()
        {
            uint start = Ptr;

            while (Data[Ptr] != 0x00)
            {
                Ptr++;
            }

            byte[] d = new byte[Ptr - start];
            Array.Copy(Data, start, d, 0, d.Length);

            return Encoding.UTF8.GetString(d);
        }

        public string readString(int offset)
        {
            string s = "";
            while (Data[offset] != 0x00)
            {
                s += (char)Data[offset];
                offset++;
            }
            return s;
        }

        public uint readUint()
        {
            return (uint)readInt();
        }

        public int readInt(int offset)
        {
            if (Endianness == Endian.Big)
                return readIntBE(offset);
            else
                return readIntLE(offset);
        }

        public int readInt()
        {
            int num;
            if (Endianness == Endian.Big)
                num = readIntBE((int)Ptr);
            else
                num = readIntLE((int)Ptr);

            Ptr += 4;

            return num;
        }

        public int readIntBE(int offset)
        {
            return ((Data[offset] & 0xFF) << 24) |
                ((Data[offset+1] & 0xFF) << 16) |
                ((Data[offset+2] & 0xFF) << 8) |
                (Data[offset+3] & 0xFF);
        }

        public int readIntLE(int offset)
        {
            return BitConverter.ToInt32(Data, offset);
        }

        public short readShort()
        {
            short num;
            if (Endianness == Endian.Big)
                num = readShortBE((int)Ptr);
            else
                num = readShortLE((int)Ptr);

            Ptr += 2;

            return num;
        }

        public short readShort(int offset)
        {
            if (Endianness == Endian.Big)
                return readShortBE(offset);
            else
                return readShortLE(offset);
        }

        public short readShortBE(int offset)
        {
            var num = ((Data[offset] & 0xFF) << 8) | (Data[offset+1] & 0xFF);
            return (short)num;
        }

        public short readShortLE(int offset)
        {
            return BitConverter.ToInt16(Data, offset);
        }

        public byte readByte()
        {
            return Data[Ptr++];
        }

        public float readFloat()
        {
            float num;

            if (Endianness == Endian.Big)
                num = readFloatBE((int)Ptr);
            else
                num = readFloatLE((int)Ptr);

            Ptr += 4;

            return num;
        }

        public float readFloat(int offset)
        {
            if (Endianness == Endian.Big)
                return readFloatBE(offset);
            else
                return readFloatLE(offset);
        }

        public float readFloatBE(int offset)
        {
            byte[] num = new byte[4] {
                Data[offset + 3],
                Data[offset + 2],
                Data[offset + 1],
                Data[offset]
            };

            return BitConverter.ToSingle(num, 0);
        }

        public float readFloatLE(int offset)
        {
            return BitConverter.ToSingle(Data, offset);
        }

        public byte[] slice(int offset, int size)
        {
            byte[] by = new byte[size];

            Array.Copy(Data, offset, by, 0, size);

            return by;
        }

        public void skip(uint size)
        {
            if (size + Ptr > Data.Length)
                throw new IndexOutOfRangeException();

            Ptr += size;
        }

        public void skipToNextWord()
        {
            skip((4 - (Ptr % 4)) % 4);
        }
    }

    public class OutputBuffer
    {
        public List<byte> Data = new List<byte>();
        public Endian Endianness = Endian.Big;

        public byte[] GetBytes()
        {
            return Data.ToArray();
        }

        public void write(byte[] d)
        {
            Data.AddRange(d);
        }

        public void write(OutputBuffer d)
        {
            Data.AddRange(d.Data);
        }

        public void writeString(string str)
        {
            char[] c = str.ToCharArray();
            for (int i = 0; i < c.Length; i++)
                Data.Add((byte)c[i]);
        }

        public void writeCString(string str)
        {
            writeString(str);
            writeByte(0);
        }

        public void writeInt(int d)
        {
            if (Endianness == Endian.Big)
                writeIntBE(d);
            else
                writeIntLE(d);
        }

        public void writeIntBE(int d)
        {
            byte[] b = BitConverter.GetBytes(d);
            Array.Reverse(b);
            write(b);
        }

        public void writeIntLE(int d)
        {
            write(BitConverter.GetBytes(d));
        }

        public void writeShort(short d)
        {
            if (Endianness == Endian.Big)
                writeShortBE(d);
            else
                writeShortLE(d);
        }

        public void writeShortBE(short d)
        {
            byte[] b = BitConverter.GetBytes(d);
            Array.Reverse(b);
            write(b);
        }

        public void writeShortLE(short d)
        {
            write(BitConverter.GetBytes(d));
        }

        public void writeFloat(float f)
        {
            if (Endianness == Endian.Big)
                writeFloatBE(f);
            else
                writeFloatLE(f);
        }

        public void writeFloatBE(float f)
        {
            byte[] b = BitConverter.GetBytes(f);
            Array.Reverse(b);
            write(b);
        }

        public void writeFloatLE(float f)
        {
            write(BitConverter.GetBytes(f));
        }

        public void writeByte(byte d)
        {
            Data.Add(d);
        }

        public byte[] getBytes()
        {
            return Data.ToArray();
        }

        public int Size
        {
            get
            {
                return Data.Count;
            }
        }
    }

}