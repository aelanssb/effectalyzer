﻿using System;
using System.Collections.Generic;
using OpenTK;
using System.IO;

namespace Effectalyzer
{
    public class PTCL
    {
        public class KEYA
        {
            public List<KEYAEntry> Entries = new List<KEYAEntry>();
        }

        public class KEYAEntry
        {
            public int Unk1;
            public int Unk2;
            public int Unk3;
            public int Unk4;
            public int Unk5;
            public int Unk6;

            public List<Vector2> Coords = new List<Vector2>();
        }

        public class Color
        {
            public float R;
            public float G;
            public float B;
            public float A;

            public static Color FromRgba (int rgba)
            {
                var color = new Color();
                color.R = ((rgba >> 24) & 0xFF) / 255.0f;
                color.G = ((rgba >> 16) & 0xFF) / 255.0f;
                color.B = ((rgba >> 8)  & 0xFF) / 255.0f;
                color.A = ((rgba     )  & 0xFF) / 255.0f;
                return color;
            }

            public override string ToString()
            {
                return $"#{(int)(R * 255):X2}{(int)(G * 255):X2}{(int)(B * 255):X2}{(int)(A * 255):X2}";
            }
        }

        public class Emitter
        {
            public ParticleSystem Parent;
            public string Name;
            public int[] TextureId = new int[3] { -1, -1, -1 };


            // TEMP
            public List<object> Data = new List<object>();
            public uint Offset;
        }

        public class ParticleSystem
        {
            public string Name;

            public int Unk01;
            public int Unk04;
            public int Unk05;
            public int Unk06;

            public List<Emitter> Emitters = new List<Emitter>();
        }

        public class FileHeader
        {
            public uint magic;
            public int unk0;
            public int numEntries;
            public int unk1;
            public int stringOffset;
            public int texturesOffset;
            public int texturesSize;
            public int shaderOffset;
            public int shaderSize;
            public int keyaOffset;
            public int keyaSize;
            public int unkOffset1;
            public int unkSize1;
            public int unk2Offset;
            public int unk2Size;
            public int unk3;
            public int unk4;
            public int unk5;
            public int unk6;
            public int unk7;
            public int unk8;
        }

        public class UnhandledChunk
        {
            public byte[] Data = new byte[] { };

            public UnhandledChunk() { }

            public UnhandledChunk(byte[] data)
            {
                Data = data;
            }

            public UnhandledChunk(byte[] data, int offset, int len)
            {
                Data = new byte[len];
                Array.Copy(data, offset, Data, 0, len);
            }
        }

        // FIXME: temp until I finish rebuilding
        public byte[] Data;

        public List<KEYA> AnimationTracks = new List<KEYA>();
        public List<ParticleSystem> Systems = new List<ParticleSystem>();
        public List<Texture> Textures = new List<Texture>();
        public UnhandledChunk Shaders = new UnhandledChunk();
        public FileHeader Header = new FileHeader();
        public string fileName;
        public string OriginalFilename;
        public UnhandledChunk UnkChunk1 = new UnhandledChunk();
        public UnhandledChunk UnkChunk2 = new UnhandledChunk();

        Dictionary<int, int> OffsetToTexIdDict = new Dictionary<int, int>();
        // TEMP
        public Dictionary<int, int> TexIdToOffsetDict = new Dictionary<int, int>();

        public class EmitterField
        {
            public string Name = null;
            public string Type = null;

            public EmitterField (string name, string type)
            {
                Name = name;
                Type = type;
            }
        }

        public static EmitterField[] EmitterFields = new EmitterField[]
        {
            new EmitterField("unk_000", "uint"),
            new EmitterField("unk_004", "uint"),
            new EmitterField("unk_008", "uint"),
            new EmitterField("unk_00C", "uint"),
            new EmitterField("unk_010", "uint"),
            new EmitterField("unk_014", "uint"),
            new EmitterField("unk_018", "uint"),
            new EmitterField("unk_01C", "uint"),
            new EmitterField("unk_020", "uint"),
            new EmitterField("unk_024", "uint"),
            new EmitterField("unk_028", "uint"),
            new EmitterField("unk_02C", "uint"),
            new EmitterField("unk_030", "uint"),
            new EmitterField("unk_034", "uint"),
            new EmitterField("nameOffset", "int"),
            new EmitterField("unk_03C", "uint"),
            new EmitterField("dimensions", "uint"),
            new EmitterField("unk_044", "uint"),
            new EmitterField("unk_048", "uint"),
            new EmitterField("unk_04C", "uint"),
            new EmitterField("unk_050", "uint"),
            new EmitterField("unk_054", "uint"),
            new EmitterField("Texture1MipLevel", "float"),
            new EmitterField("unk_05C", "uint"),
            new EmitterField("HasTexture1", "int"),
            new EmitterField("unk_064", "uint"),
            new EmitterField("unk_068", "uint"),
            new EmitterField("Texture1Format", "int"),
            new EmitterField("Texture1Size", "uint"),
            new EmitterField("Texture1Offset", "uint"),
            new EmitterField("unk_078", "uint"),
            new EmitterField("unk_07C", "uint"),
            new EmitterField("unk_080", "uint"),
            new EmitterField("unk_084", "uint"),
            new EmitterField("unk_088", "uint"),
            new EmitterField("unk_08C", "uint"),
            new EmitterField("unk_090", "uint"),
            new EmitterField("unk_094", "uint"),
            new EmitterField("unk_098", "uint"),
            new EmitterField("unk_09C", "uint"),
            new EmitterField("unk_0A0", "uint"),
            new EmitterField("unk_0A4", "uint"),
            new EmitterField("unk_0A8", "uint"),
            new EmitterField("unk_0AC", "uint"),
            new EmitterField("unk_0B0", "uint"),
            new EmitterField("unk_0B4", "uint"),
            new EmitterField("unk_0B8", "uint"),
            new EmitterField("unk_0BC", "uint"),
            new EmitterField("unk_0C0", "uint"),
            new EmitterField("unk_0C4", "uint"),
            new EmitterField("unk_0C8", "uint"),
            new EmitterField("unk_0CC", "uint"),
            new EmitterField("unk_0D0", "uint"),
            new EmitterField("unk_0D4", "uint"),
            new EmitterField("unk_0D8", "uint"),
            new EmitterField("unk_0DC", "uint"),
            new EmitterField("unk_0E0", "uint"),
            new EmitterField("unk_0E4", "uint"),
            new EmitterField("unk_0E8", "uint"),
            new EmitterField("unk_0EC", "uint"),
            new EmitterField("unk_0F0", "uint"),
            new EmitterField("unk_0F4", "uint"),
            new EmitterField("unk_0F8", "uint"),
            new EmitterField("unk_0FC", "uint"),
            new EmitterField("unk_100", "uint"),
            new EmitterField("unk_104", "uint"),
            new EmitterField("unk_108", "uint"),
            new EmitterField("unk_10C", "uint"),
            new EmitterField("unk_110", "uint"),
            new EmitterField("unk_114", "uint"),
            new EmitterField("unk_118", "uint"),
            new EmitterField("unk_11C", "uint"),
            new EmitterField("unk_120", "uint"),
            new EmitterField("unk_124", "uint"),
            new EmitterField("unk_128", "uint"),
            new EmitterField("unk_12C", "uint"),
            new EmitterField("Texture2MipLevel", "float"),
            new EmitterField("unk_134", "uint"),
            new EmitterField("HasTexture2", "int"),
            new EmitterField("unk_13C", "uint"),
            new EmitterField("unk_140", "uint"),
            new EmitterField("Texture2Format", "int"),
            new EmitterField("Texture2Size", "uint"),
            new EmitterField("Texture2Offset", "uint"),
            new EmitterField("unk_150", "uint"),
            new EmitterField("unk_154", "uint"),
            new EmitterField("unk_158", "uint"),
            new EmitterField("unk_15C", "uint"),
            new EmitterField("unk_160", "uint"),
            new EmitterField("unk_164", "uint"),
            new EmitterField("unk_168", "uint"),
            new EmitterField("unk_16C", "uint"),
            new EmitterField("unk_170", "uint"),
            new EmitterField("unk_174", "uint"),
            new EmitterField("unk_178", "uint"),
            new EmitterField("unk_17C", "uint"),
            new EmitterField("unk_180", "uint"),
            new EmitterField("unk_184", "uint"),
            new EmitterField("unk_188", "uint"),
            new EmitterField("unk_18C", "uint"),
            new EmitterField("unk_190", "uint"),
            new EmitterField("unk_194", "uint"),
            new EmitterField("unk_198", "uint"),
            new EmitterField("unk_19C", "uint"),
            new EmitterField("unk_1A0", "uint"),
            new EmitterField("unk_1A4", "uint"),
            new EmitterField("unk_1A8", "uint"),
            new EmitterField("unk_1AC", "uint"),
            new EmitterField("unk_1B0", "uint"),
            new EmitterField("unk_1B4", "uint"),
            new EmitterField("unk_1B8", "uint"),
            new EmitterField("unk_1BC", "uint"),
            new EmitterField("unk_1C0", "uint"),
            new EmitterField("unk_1C4", "uint"),
            new EmitterField("unk_1C8", "uint"),
            new EmitterField("unk_1CC", "uint"),
            new EmitterField("unk_1D0", "uint"),
            new EmitterField("unk_1D4", "uint"),
            new EmitterField("unk_1D8", "uint"),
            new EmitterField("unk_1DC", "uint"),
            new EmitterField("unk_1E0", "uint"),
            new EmitterField("unk_1E4", "uint"),
            new EmitterField("unk_1E8", "uint"),
            new EmitterField("unk_1EC", "uint"),
            new EmitterField("unk_1F0", "uint"),
            new EmitterField("unk_1F4", "uint"),
            new EmitterField("unk_1F8", "uint"),
            new EmitterField("unk_1FC", "uint"),
            new EmitterField("unk_200", "uint"),
            new EmitterField("unk_204", "uint"),
            new EmitterField("Texture3MipLevel", "float"),
            new EmitterField("unk_20C", "uint"),
            new EmitterField("HasTexture3", "int"),
            new EmitterField("unk_214", "uint"),
            new EmitterField("unk_218", "uint"),
            new EmitterField("Texture3Format", "int"),
            new EmitterField("Texture3Size", "uint"),
            new EmitterField("Texture3Offset", "uint"),
            new EmitterField("unk_228", "uint"),
            new EmitterField("unk_22C", "uint"),
            new EmitterField("unk_230", "uint"),
            new EmitterField("unk_234", "uint"),
            new EmitterField("unk_238", "uint"),
            new EmitterField("unk_23C", "uint"),
            new EmitterField("unk_240", "uint"),
            new EmitterField("unk_244", "uint"),
            new EmitterField("unk_248", "uint"),
            new EmitterField("unk_24C", "uint"),
            new EmitterField("unk_250", "uint"),
            new EmitterField("unk_254", "uint"),
            new EmitterField("unk_258", "uint"),
            new EmitterField("unk_25C", "uint"),
            new EmitterField("unk_260", "uint"),
            new EmitterField("unk_264", "uint"),
            new EmitterField("unk_268", "uint"),
            new EmitterField("unk_26C", "uint"),
            new EmitterField("unk_270", "uint"),
            new EmitterField("unk_274", "uint"),
            new EmitterField("unk_278", "uint"),
            new EmitterField("unk_27C", "uint"),
            new EmitterField("unk_280", "uint"),
            new EmitterField("unk_284", "uint"),
            new EmitterField("unk_288", "uint"),
            new EmitterField("unk_28C", "uint"),
            new EmitterField("unk_290", "uint"),
            new EmitterField("unk_294", "uint"),
            new EmitterField("unk_298", "uint"),
            new EmitterField("unk_29C", "uint"),
            new EmitterField("unk_2A0", "uint"),
            new EmitterField("unk_2A4", "uint"),
            new EmitterField("unk_2A8", "uint"),
            new EmitterField("unk_2AC", "uint"),
            new EmitterField("unk_2B0", "uint"),
            new EmitterField("unk_2B4", "uint"),
            new EmitterField("unk_2B8", "uint"),
            new EmitterField("unk_2BC", "uint"),
            new EmitterField("unk_2C0", "uint"),
            new EmitterField("unk_2C4", "uint"),
            new EmitterField("unk_2C8", "uint"),
            new EmitterField("unk_2CC", "uint"),
            new EmitterField("ScaleanimationType?", "uint"),
            new EmitterField("unk_2D4", "uint"),
            new EmitterField("unk_2D8", "uint"),
            new EmitterField("unk_2DC", "uint"),
            new EmitterField("unk_2E0", "uint"),
            new EmitterField("unk_2E4", "uint"),
            new EmitterField("unk_2E8", "uint"),
            new EmitterField("unk_2EC", "uint"),
            new EmitterField("unk_2F0", "uint"),
            new EmitterField("unk_2F4", "uint"),
            new EmitterField("unk_2F8", "uint"),
            new EmitterField("unk_2FC", "uint"),
            new EmitterField("unk_300", "uint"),
            new EmitterField("unk_304", "uint"),
            new EmitterField("unk_308", "uint"),
            new EmitterField("unk_30C", "uint"),
            new EmitterField("unk_310", "uint"),
            new EmitterField("unk_314", "uint"),
            new EmitterField("unk_318", "uint"),
            new EmitterField("unk_31C", "uint"),
            new EmitterField("unk_320", "uint"),
            new EmitterField("unk_324", "uint"),
            new EmitterField("unk_328", "uint"),
            new EmitterField("ScaleAnimationId", "int"),
            new EmitterField("unk_330", "uint"),
            new EmitterField("unk_334", "uint"),
            new EmitterField("unk_338", "uint"),
            new EmitterField("unk_33C", "uint"),
            new EmitterField("EmitterShape", "int"),
            new EmitterField("unk_344", "uint"),
            new EmitterField("float_348", "float"),
            new EmitterField("float_34C", "float"),
            new EmitterField("float_350", "float"),
            new EmitterField("float_354", "float"),
            new EmitterField("float_358", "float"),
            new EmitterField("unk_35C", "uint"),
            new EmitterField("float_360", "float"),
            new EmitterField("float_364", "float"),
            new EmitterField("float_368", "float"),
            new EmitterField("float_36C", "float"),
            new EmitterField("unk_370", "uint"),
            new EmitterField("unk_374", "uint"),
            new EmitterField("float_378", "float"),
            new EmitterField("float_37C", "float"),
            new EmitterField("float_380", "float"),
            new EmitterField("float_384", "float"),
            new EmitterField("float_388", "float"),
            new EmitterField("float_38C", "float"),
            new EmitterField("float_390", "float"),
            new EmitterField("float_394", "float"),
            new EmitterField("float_398", "float"),
            new EmitterField("float_39C", "float"),
            new EmitterField("float_3A0", "float"),
            new EmitterField("float_3A4", "float"),
            new EmitterField("unk_3A8", "uint"),
            new EmitterField("unk_3AC", "uint"),
            new EmitterField("unk_3B0", "uint"),
            new EmitterField("unk_3B4", "uint"),
            new EmitterField("unk_3B8", "uint"),
            new EmitterField("unk_3BC", "uint"),
            new EmitterField("unk_3C0", "uint"),
            new EmitterField("unk_3C4", "uint"),
            new EmitterField("unk_3C8", "uint"),
            new EmitterField("unk_3CC", "uint"),
            new EmitterField("unk_3D0", "uint"),
            new EmitterField("unk_3D4", "uint"),
            new EmitterField("float_3D8", "float"),
            new EmitterField("unk_3DC", "uint"),
            new EmitterField("unk_3E0", "uint"),
            new EmitterField("float_3E4", "float"),
            new EmitterField("unk_3E8", "uint"),
            new EmitterField("float_3EC", "float"),
            new EmitterField("float_3F0", "float"),
            new EmitterField("float_3F4", "float"),
            new EmitterField("unk_3F8", "uint"),
            new EmitterField("float_3FC", "float"),
            new EmitterField("float_400", "float"),
            new EmitterField("unk_404", "uint"),
            new EmitterField("float_408", "float"),
            new EmitterField("float_40C", "float"),
            new EmitterField("float_410", "float"),
            new EmitterField("float_414", "float"),
            new EmitterField("unk_418", "uint"),
            new EmitterField("unk_41C", "uint"),
            new EmitterField("float_420", "float"),
            new EmitterField("float_424", "float"),
            new EmitterField("unk_428", "uint"),
            new EmitterField("unk_42C", "uint"),
            new EmitterField("unk_430", "uint"),
            new EmitterField("unk_434", "uint"),
            new EmitterField("unk_438", "uint"),
            new EmitterField("unk_43C", "uint"),
            new EmitterField("unk_440", "uint"),
            new EmitterField("float_444", "float"),
            new EmitterField("unk_448", "uint"),
            new EmitterField("unk_44C", "uint"),
            new EmitterField("SpawnRate?", "uint"),
            new EmitterField("unk_454", "uint"),
            new EmitterField("unk_458", "uint"),
            new EmitterField("float_45C", "float"),
            new EmitterField("unk_460", "uint"),
            new EmitterField("float_464", "float"),
            new EmitterField("unk_468", "uint"),
            new EmitterField("float_46C", "float"),
            new EmitterField("float_470", "float"),
            new EmitterField("float_474", "float"),
            new EmitterField("ColorScale", "float"),
            new EmitterField("unk_47C", "uint"),
            new EmitterField("unk_480", "uint"),
            new EmitterField("float_484", "float"),
            new EmitterField("unk_488", "uint"),
            new EmitterField("unk_48C", "uint"),
            new EmitterField("unk_490", "uint"),
            new EmitterField("float_494", "float"),
            new EmitterField("float_498", "float"),
            new EmitterField("unk_49C", "uint"),
            new EmitterField("unk_4A0", "uint"),
            new EmitterField("unk_4A4", "uint"),
            new EmitterField("unk_4A8", "uint"),
            new EmitterField("unk_4AC", "uint"),
            new EmitterField("Lifetime", "int"),
            new EmitterField("unk_4B4", "uint"),
            new EmitterField("unk_4B8", "uint"),
            new EmitterField("unk_4BC", "uint"),
            new EmitterField("unk_4C0", "uint"),
            new EmitterField("unk_4C4", "uint"),
            new EmitterField("float_4C8", "float"),
            new EmitterField("unk_4CC", "uint"),
            new EmitterField("unk_4D0", "uint"),
            new EmitterField("unk_4D4", "uint"),
            new EmitterField("unk_4D8", "uint"),
            new EmitterField("unk_4DC", "uint"),
            new EmitterField("unk_4E0", "uint"),
            new EmitterField("unk_4E4", "uint"),
            new EmitterField("unk_4E8", "uint"),
            new EmitterField("unk_4EC", "uint"),
            new EmitterField("unk_4F0", "uint"),
            new EmitterField("unk_4F4", "uint"),
            new EmitterField("unk_4F8", "uint"),
            new EmitterField("unk_4FC", "uint"),
            new EmitterField("unk_500", "uint"),
            new EmitterField("unk_504", "uint"),
            new EmitterField("Texture1RepeatX", "float"),
            new EmitterField("Texture1RepeatY", "float"),
            new EmitterField("unk_510", "uint"),
            new EmitterField("unk_514", "uint"),
            new EmitterField("unk_518", "uint"),
            new EmitterField("unk_51C", "uint"),
            new EmitterField("unk_520", "uint"),
            new EmitterField("unk_524", "uint"),
            new EmitterField("unk_528", "uint"),
            new EmitterField("unk_52C", "uint"),
            new EmitterField("unk_530", "uint"),
            new EmitterField("float_534", "float"),
            new EmitterField("float_538", "float"),
            new EmitterField("unk_53C", "uint"),
            new EmitterField("unk_540", "uint"),
            new EmitterField("unk_544", "uint"),
            new EmitterField("unk_548", "uint"),
            new EmitterField("unk_54C", "uint"),
            new EmitterField("unk_550", "uint"),
            new EmitterField("unk_554", "uint"),
            new EmitterField("unk_558", "uint"),
            new EmitterField("unk_55C", "uint"),
            new EmitterField("unk_560", "uint"),
            new EmitterField("unk_564", "uint"),
            new EmitterField("unk_568", "uint"),
            new EmitterField("unk_56C", "uint"),
            new EmitterField("unk_570", "uint"),
            new EmitterField("unk_574", "uint"),
            new EmitterField("unk_578", "uint"),
            new EmitterField("unk_57C", "uint"),
            new EmitterField("unk_580", "uint"),
            new EmitterField("Texture2RepeatX", "float"),
            new EmitterField("Texture2RepeatY", "float"),
            new EmitterField("unk_58C", "uint"),
            new EmitterField("unk_590", "uint"),
            new EmitterField("unk_594", "uint"),
            new EmitterField("unk_598", "uint"),
            new EmitterField("unk_59C", "uint"),
            new EmitterField("unk_5A0", "uint"),
            new EmitterField("unk_5A4", "uint"),
            new EmitterField("unk_5A8", "uint"),
            new EmitterField("unk_5AC", "uint"),
            new EmitterField("float_5B0", "float"),
            new EmitterField("float_5B4", "float"),
            new EmitterField("unk_5B8", "uint"),
            new EmitterField("unk_5BC", "uint"),
            new EmitterField("unk_5C0", "uint"),
            new EmitterField("unk_5C4", "uint"),
            new EmitterField("unk_5C8", "uint"),
            new EmitterField("unk_5CC", "uint"),
            new EmitterField("unk_5D0", "uint"),
            new EmitterField("unk_5D4", "uint"),
            new EmitterField("unk_5D8", "uint"),
            new EmitterField("unk_5DC", "uint"),
            new EmitterField("unk_5E0", "uint"),
            new EmitterField("unk_5E4", "uint"),
            new EmitterField("unk_5E8", "uint"),
            new EmitterField("unk_5EC", "uint"),
            new EmitterField("unk_5F0", "uint"),
            new EmitterField("unk_5F4", "uint"),
            new EmitterField("unk_5F8", "uint"),
            new EmitterField("unk_5FC", "uint"),
            new EmitterField("Texture3RepeatX", "float"),
            new EmitterField("Texture3RepeatY", "float"),
            new EmitterField("unk_608", "uint"),
            new EmitterField("unk_60C", "uint"),
            new EmitterField("unk_610", "uint"),
            new EmitterField("unk_614", "uint"),
            new EmitterField("unk_618", "uint"),
            new EmitterField("unk_61C", "uint"),
            new EmitterField("unk_620", "uint"),
            new EmitterField("unk_624", "uint"),
            new EmitterField("unk_628", "uint"),
            new EmitterField("float_62C", "float"),
            new EmitterField("float_630", "float"),
            new EmitterField("unk_634", "uint"),
            new EmitterField("unk_638", "uint"),
            new EmitterField("unk_63C", "uint"),
            new EmitterField("unk_640", "uint"),
            new EmitterField("unk_644", "uint"),
            new EmitterField("color_1_type", "int"),
            new EmitterField("color_2_type", "int"),
            new EmitterField("color_1_1", "color"),
            new EmitterField("color_1_2", "color"),
            new EmitterField("color_1_3", "color"),
            new EmitterField("color_1_4", "color"),
            new EmitterField("color_1_5", "color"),
            new EmitterField("color_1_6", "color"),
            new EmitterField("color_1_7", "color"),
            new EmitterField("color_1_8", "color"),
            new EmitterField("color_2_1", "color"),
            new EmitterField("color_2_2", "color"),
            new EmitterField("color_2_3", "color"),
            new EmitterField("color_2_4", "color"),
            new EmitterField("color_2_5", "color"),
            new EmitterField("color_2_6", "color"),
            new EmitterField("color_2_7", "color"),
            new EmitterField("color_2_8", "color"),
            new EmitterField("unk_750", "uint"),
            new EmitterField("unk_754", "uint"),
            new EmitterField("unk_758", "uint"),
            new EmitterField("unk_75C", "uint"),
            new EmitterField("unk_760", "uint"),
            new EmitterField("unk_764", "uint"),
            new EmitterField("color_1_randomCount", "int"),
            new EmitterField("color_2_randomCount", "int"),
            new EmitterField("unk_770", "uint"),
            new EmitterField("unk_774", "uint"),
            new EmitterField("float_778", "float"),
            new EmitterField("unk_77C", "uint"),
            new EmitterField("unk_780", "uint"),
            new EmitterField("float_784", "float"),
            new EmitterField("float_788", "float"),
            new EmitterField("float_78C", "float"),
            new EmitterField("float_790", "float"),
            new EmitterField("float_794", "float"),
            new EmitterField("float_798", "float"),
            new EmitterField("unk_79C", "uint"),
            new EmitterField("unk_7A0", "uint"),
            new EmitterField("unk_7A4", "uint"),
            new EmitterField("float_7A8", "float"),
            new EmitterField("unk_7AC", "uint"),
            new EmitterField("unk_7B0", "uint"),
            new EmitterField("unk_7B4", "uint"),
            new EmitterField("unk_7B8", "uint"),
            new EmitterField("unk_7BC", "uint"),
            new EmitterField("unk_7C0", "uint"),
            new EmitterField("unk_7C4", "uint"),
            new EmitterField("unk_7C8", "uint"),
            new EmitterField("unk_7CC", "uint"),
            new EmitterField("unk_7D0", "uint"),
            new EmitterField("SizeX", "float"),
            new EmitterField("SizeY", "float"),
            new EmitterField("SizeZ", "float"),
            new EmitterField("ScaleX", "float"),
            new EmitterField("ScaleY", "float"),
            new EmitterField("ScaleZ", "float"),
            new EmitterField("TexRotationX", "float"),
            new EmitterField("TexRotationY", "float"),
            new EmitterField("TexRotationZ", "float"),
            new EmitterField("float_7F8", "float"),
            new EmitterField("unk_7FC", "uint"),
            new EmitterField("unk_800", "uint"),
            new EmitterField("unk_804", "uint"),
            new EmitterField("unk_808", "uint"),
            new EmitterField("unk_80C", "uint"),
            new EmitterField("unk_810", "uint"),
            new EmitterField("unk_814", "uint"),
            new EmitterField("float_818", "float"),
            new EmitterField("unk_81C", "uint"),
            new EmitterField("unk_820", "uint"),
            new EmitterField("unk_824", "uint"),
            new EmitterField("float_828", "float"),
            new EmitterField("float_82C", "float"),
            new EmitterField("unk_830", "uint"),
            new EmitterField("unk_834", "uint"),
            new EmitterField("unk_838", "uint"),
            new EmitterField("unk_83C", "uint"),
            new EmitterField("unk_840", "uint"),
            new EmitterField("ShaderId", "int"),
            new EmitterField("unk_848", "uint"),
            new EmitterField("unk_84C", "uint"),
            new EmitterField("unk_850", "uint"),
            new EmitterField("unk_854", "uint"),
            new EmitterField("unk_858", "uint"),
            new EmitterField("unk_85C", "uint"),
            new EmitterField("float_860", "float"),
            new EmitterField("float_864", "float"),
            new EmitterField("float_868", "float"),
            new EmitterField("float_86C", "float"),
            new EmitterField("float_870", "float"),
            new EmitterField("float_874", "float"),
            new EmitterField("float_878", "float"),
            new EmitterField("unk_87C", "uint"),
            new EmitterField("unk_880", "uint"),
            new EmitterField("unk_884", "uint"),
            new EmitterField("unk_888", "uint"),
            new EmitterField("unk_88C", "uint"),
            new EmitterField("unk_890", "uint"),
            new EmitterField("unk_894", "uint"),
            new EmitterField("unk_898", "uint"),
            new EmitterField("unk_89C", "uint"),
            new EmitterField("unk_8A0", "uint"),
            new EmitterField("unk_8A4", "uint"),
            new EmitterField("float_8A8", "float"),
            new EmitterField("unk_8AC", "uint"),
            new EmitterField("unk_8B0", "uint"),
            new EmitterField("float_8B4", "float"),
            new EmitterField("float_8B8", "float"),
        };

        public PTCL() { }

        public PTCL (string filename)
        {
            OriginalFilename = filename;
            var file = new InputBuffer(filename);

            Data = file.Data;

            Header.magic = (uint)file.readInt();
            if (Header.magic == 0x53504244) // SPBD (3ds)
                file.Endianness = Endian.Little;

            Header.unk0 = file.readInt();
            Header.numEntries = file.readInt();
            Header.unk1 = file.readInt();
            Header.stringOffset = file.readInt();
            Header.texturesOffset = file.readInt();
            Header.texturesSize = file.readInt();
            Header.shaderOffset = file.readInt();
            Header.shaderSize = file.readInt();
            Header.keyaOffset = file.readInt();
            Header.keyaSize = file.readInt();
            Header.unkOffset1 = file.readInt();
            Header.unkSize1 = file.readInt();
            Header.unk2Offset = file.readInt();
            Header.unk2Size = file.readInt();
            Header.unk3 = file.readInt(); // count of.. something. can't recall.
            Header.unk4 = file.readInt();
            Header.unk5 = file.readInt(); // 0x40
            Header.unk6 = file.readInt();
            Header.unk7 = file.readInt();
            Header.unk8 = file.readInt();

            //
            var obuf = new OutputBuffer();
            file.Ptr = 0x50;
            for (int i = 0; i < Header.numEntries; i++)
            {
                var system = new ParticleSystem();
                system.Name = file.readString(Header.stringOffset + file.readInt());
                system.Unk01 = file.readInt();
                var emitterCount = file.readInt();
                var emittersOffset = file.readInt();
                system.Unk04 = file.readInt();
                system.Unk05 = file.readInt();
                system.Unk06 = file.readInt();

                obuf.writeString($"0x{i:X2}: {system.Name}\n");

                for (int j = 0; j < emitterCount; j++)
                {
                    var emitter = new Emitter();
                    emitter.Parent = system;
                    var emitterDataOffset = file.readInt(emittersOffset + (j * 0x10));
                    emitter.Offset = (uint)emitterDataOffset;

                    emitter.Name = file.readString(Header.stringOffset + file.readInt(emitterDataOffset + 0x38));
                    var childWidth = file.readShort(emitterDataOffset + 0x40);
                    var childHeight = file.readShort(emitterDataOffset + 0x42);
                    var keyaOffset = file.readShort(emitterDataOffset + 0x2CC);

                    var shaderId = file.readInt(emitterDataOffset + 0x844);

                    system.Emitters.Add(emitter);

                    // I can't for the life of me remember where the name "sht" came from.
                    // Shaders, perhaps?
                    for (int shtId = 0; shtId < 3; shtId++)
                    {
                        var shtOffset = emitterDataOffset + 0x58 + (shtId * 0xD8);

                        // FIXME: please explain wtf this is maybe?
                        if (file.readInt(shtOffset + 0x08) == 0)
                            continue;

                        var textureFormat = file.readInt(shtOffset + 0x14);

                        if (textureFormat == 0)
                            continue;

                        var childTextureSize = file.readInt(shtOffset + 0x18);
                        var childTextureOffset = file.readInt(shtOffset + 0x1C);
                        if (!OffsetToTexIdDict.ContainsKey(childTextureOffset))
                        {
                            var tex = new Texture();
                            tex.Width = (childWidth >>= shtId);
                            tex.Height = (childHeight >>= shtId);
                            tex.OriginalSize = childTextureSize;
                            //tex.FullData = file.read(Header.texturesOffset + childTextureOffset, childTextureSize);

                            tex.Format = texFormatToSurfaceFormat(textureFormat);

                            int mipSize = largestContainedPoT(childTextureSize);
                            int mipLevel = 0;
                            int mipOffset = 0;

                            // TODO: locate mip count in emitter. assuming that's a thing.
                            while (true)
                            {
                                if (mipSize == 1)
                                    break;

                                if ((childTextureSize & mipSize) == mipSize)
                                {

                                    var mipData = file.read(Header.texturesOffset + childTextureOffset + mipOffset, childTextureSize - mipOffset);
                                    AddrLib.Swizzle(tex, mipData, mipLevel);

                                    mipOffset += mipSize;
                                }

                                mipSize >>= 1;
                                mipLevel++;
                            }

                            tex.BufferTexture();

                            var id = Textures.Count;
                            OffsetToTexIdDict.Add(childTextureOffset, id);
                            TexIdToOffsetDict.Add(id, childTextureOffset);
                            Textures.Add(tex);
                        }

                        emitter.TextureId[shtId] = OffsetToTexIdDict[childTextureOffset];
                    }

                    obuf.writeString($"\t0x{j:X2}: {system.Name}__{emitter.Name}\n");
                    int k = 0;
                    foreach (var field in EmitterFields)
                    {
                        var kAbs = (emitterDataOffset + k);

                        switch (field.Type)
                        {
                            case "float":
                            {
                                var num = file.readFloat(kAbs);
                                emitter.Data.Add(num);
                                obuf.writeString($"\t\t0x{k:X4}: {field.Name} = {num:f1} # offs=0x{kAbs:X8}\n");
                            }
                            break;

                            case "color":
                            {
                                var color = new Vector4(
                                    file.readFloat(kAbs + 0x00),
                                    file.readFloat(kAbs + 0x04),
                                    file.readFloat(kAbs + 0x08),
                                    file.readFloat(kAbs + 0x0C)
                                );
                            
                                emitter.Data.Add(color);
                                obuf.writeString($"\t\t0x{k:X4}: {field.Name} = ({color}) # offs=0x{kAbs:X8}\n");
                                k += 0x0C;
                            }
                            break;

                            default:
                            {
                                var num = file.readInt(kAbs);
                                emitter.Data.Add(num);
                                obuf.writeString($"\t\t0x{k:X4}: {field.Name} = 0x{num:X8} # offs=0x{kAbs:X8}\n");
                            }
                            break;
                        }
                    }
                }

                Systems.Add(system);
                obuf.writeString("\n\n");
            }

            // this is handy for re. nothin' else.
#if DEBUG
            using (var os = new FileStream("D:/what.txt", FileMode.Create))
            {
                os.Write(obuf.GetBytes(), 0, obuf.Size);
            }
#endif
            file.Ptr = (uint)Header.stringOffset;
            fileName = file.readString();

            ////
            Shaders = new UnhandledChunk(file.read(Header.shaderOffset, Header.shaderSize));
            //{
            file.Ptr = (uint)Header.shaderOffset;

            int numShaders = file.readInt();
            int unkOffset0 = file.readInt();
            int unk0 = file.readInt();
            int unkOffset1 = file.readInt();
            int dataOffset = file.readInt();
            int footerOffset = file.readInt();

            file.Ptr = (uint)(Header.shaderOffset + dataOffset);
            numShaders = 0; // TEMP
            for (int shaderId = 0; shaderId < numShaders; shaderId++)
            {
                int of = Header.shaderOffset + footerOffset + (shaderId * 0x78);
                int size = file.readInt(of + 0x68);
                int offset = file.readInt(of + 0x6C);

                byte[] shaderData = file.read(Header.shaderOffset + dataOffset + offset, size);

#if SHADER_DUMP
                var fn = $"D:/test/shader_{shaderId}.gsh";
                using (var ofile = File.OpenWrite(fn))
                {
                    ofile.Write(shaderData, 0, size);
                }

                var xyzzy = new OutputBuffer();
                xyzzy.writeString(Shell("D:/test/", "gfd-tool", "info " + fn));

                using (var ofile = File.OpenWrite(fn + ".txt"))
                {
                    ofile.Write(xyzzy.GetBytes(), 0, xyzzy.Size);
                }
#endif
            }

            file.Ptr = (uint)Header.keyaOffset;
            while (true)
            {
                uint keyaMagic = (uint)file.readInt();

                if (keyaMagic != 0x4B455941) // "KEYA", if you can believe it.
                {
                    file.Ptr -= 4;
                    break;
                }

                var keyframe = new KEYA();
                int numKeyaEntries = file.readInt();

                for (int i = 0; i < numKeyaEntries; i++)
                {
                    KEYAEntry entry = new KEYAEntry();
                    var numCoords = file.readInt();
                    entry.Unk1 = file.readInt();
                    entry.Unk2 = file.readInt();
                    entry.Unk3 = file.readInt();
                    entry.Unk4 = file.readInt();
                    entry.Unk5 = file.readInt();
                    entry.Unk6 = file.readInt();

                    for (int j = 0; j < numCoords; j++)
                    {
                        entry.Coords.Add(new Vector2(file.readFloat(), file.readFloat()));
                    }

                    keyframe.Entries.Add(entry);
                }

                AnimationTracks.Add(keyframe);
            }

            UnkChunk1 = new UnhandledChunk(file.read(Header.unkOffset1, Header.unkSize1));

            if (Header.unk2Size > 0)
                UnkChunk2 = new UnhandledChunk(file.read(Header.unk2Offset, Header.unk2Size));

#if DEBUG
            //CompareShit(Systems[2].Emitters[4], Systems[2].Emitters[5], false);
#endif
        }

        private GTX.SurfaceFormat texFormatToSurfaceFormat(int textureFormat)
        {
            switch (textureFormat)
            {
                case 0x03:
                return GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC1_UNORM;
                case 0x04:
                return GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC1_SRGB;
                case 0x05:
                return GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC2_UNORM;
                case 0x06:
                return GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC2_SRGB;
                case 0x07:
                return GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC3_UNORM;
                case 0x08:
                return GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC3_SRGB;
                case 0x09:
                return GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC4_UNORM;
                case 0x0A:
                return GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC4_SNORM;
                case 0x0B:
                return GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC5_UNORM;
                case 0x0C:
                return GTX.SurfaceFormat.GX2_SURFACE_FORMAT_T_BC5_SNORM;

                default:
                throw new NotImplementedException($"Unhandled texture format {textureFormat}. Tell moat or this will never be fixed. thx");
            }
        }

        public void CompareShit(Emitter a, Emitter b, bool same)
        {
            var obuf = new OutputBuffer();

            int i = 0;
            foreach (var worda in a.Data)
            {
                if ((worda == b.Data[i]) == same)
                {
                    obuf.writeString($"0x{i * 4:X4}: {EmitterFields[i*4].Name} 0x{worda:X8}  vs  0x{b.Data[i]:X8}\n");
                }
                i++;
            }

            using (var os = new FileStream("D:/test/diff.txt", FileMode.Create))
            {
                os.Write(obuf.GetBytes(), 0, obuf.Size);
            }
        }

        public int largestContainedPoT(int num)
        {
            uint unum = (uint)num;
            for (uint pot = 0x80000000; pot > 1; pot >>= 1)
            {
                if ((unum & pot) == pot)
                    return (int)pot;
            }

            return -1;
        }

        // TODO: This is the most important thing to finish.
        // Without this working correctly, editing files is a waste of time!
        public byte[] Rebuild()
        {
            foreach (var system in Systems)
            {
                foreach (var emitter in system.Emitters)
                {
                    var newEmitterBuf = new OutputBuffer();

                    int i = 0;
                    foreach (var field in EmitterFields)
                    {
                        object value = emitter.Data[i++];

                        switch (field.Type)
                        {
                            case "int":
                            case "uint":
                            newEmitterBuf.writeInt((int)value);
                            break;

                            case "float":
                            newEmitterBuf.writeFloat((float)value);
                            break;

                            //case "vec4":
                            case "color":
                            {
                                var color = (Vector4)value;
                                newEmitterBuf.writeFloat(color.X);
                                newEmitterBuf.writeFloat(color.Y);
                                newEmitterBuf.writeFloat(color.Z);
                                newEmitterBuf.writeFloat(color.W);
                            }
                            break;

                            default:
                            throw new NotImplementedException($"Invalid type \"{field.Type}\"");
                        }
                    }

                    var newEmitterData = newEmitterBuf.GetBytes();
                    Array.Copy(newEmitterData, 0, Data, emitter.Offset, newEmitterData.Length);
                }
            }

            foreach (var kv in TexIdToOffsetDict)
            {
                var off = (Header.texturesOffset + kv.Value);
                var tex = Textures[kv.Key];

                int mipSize = largestContainedPoT(tex.OriginalSize);
                int mipLevel = 0;

                // TODO: locate mip count in emitter. assuming that's a thing.
                while (true)
                {
                    if (mipSize == 1)
                        break;

                    if ((tex.OriginalSize & mipSize) == mipSize)
                    {
                        var mipData = AddrLib.ReSwizzle(tex, mipLevel++);
                        Array.Copy(mipData, 0, Data, off, mipData.Length);

                        off += mipSize;
                    }

                    mipSize >>= 1;
                }
            }

            return Data;

            //var o = new OutputBuffer();
            //var particlesBuf = new OutputBuffer();
            //var stringsBuf = new OutputBuffer();
            //var texturesBuf = new OutputBuffer();
            //var shadersBuf = new OutputBuffer();
            //var keyaBuf = new OutputBuffer();

            //// strings
            //var strToOffsetDict = new Dictionary<string, int>();
            //int offs = 0;
            //stringsBuf.writeCString(fileName);
            //foreach (var system in Systems)
            //{
            //    if (!strToOffsetDict.ContainsKey(system.Name))
            //    {
            //        strToOffsetDict.Add(system.Name, offs);
            //        stringsBuf.writeCString(system.Name);
            //        offs += (system.Name.Length + 1);
            //    }

            //    foreach (var emitter in system.Emitters)
            //    {
            //        if (!strToOffsetDict.ContainsKey(emitter.Name))
            //        {
            //            strToOffsetDict.Add(emitter.Name, offs);
            //            stringsBuf.writeCString(emitter.Name);
            //            offs += (emitter.Name.Length + 1);
            //        }
            //    }
            //}
            //var padLen = (4 - (stringsBuf.Size % 4)) % 4;
            //stringsBuf.write(new byte[padLen]);

            //// textures
            //foreach (var tex in Textures)
            //{
            //    // TODO: texture rebuilding lmao
            //    texturesBuf.write(tex.FullData);
            //}

            //// shaders
            //shadersBuf.write(Shaders.Data);

            //// keya
            //keyaBuf.writeInt(0x4B455941); // "KEYA"
            //foreach (var keya in Keyframes)
            //{
            //    keyaBuf.writeInt(keya.Entries.Count);
            //    foreach (var entry in keya.Entries)
            //    {
            //        keyaBuf.writeInt(entry.Coords.Count);
            //        keyaBuf.writeInt(entry.Unk1);
            //        keyaBuf.writeInt(entry.Unk2);
            //        keyaBuf.writeInt(entry.Unk3);
            //        keyaBuf.writeInt(entry.Unk4);
            //        keyaBuf.writeInt(entry.Unk5);
            //        keyaBuf.writeInt(entry.Unk6);

            //        foreach (var coord in entry.Coords)
            //        {
            //            keyaBuf.writeFloat(coord.X);
            //            keyaBuf.writeFloat(coord.Y);
            //        }
            //    }
            //}

            //// build particle systems.
            //foreach (var system in Systems)
            //{
            //    particlesBuf.writeInt(strToOffsetDict[system.Name]);
            //    particlesBuf.writeInt(system.Unk01);
            //    particlesBuf.writeInt(system.Emitters.Count);
            //    //particlesBuf.writeInt(emittersOffset);
            //    particlesBuf.writeInt(system.Unk04);
            //    particlesBuf.writeInt(system.Unk05);
            //    particlesBuf.writeInt(system.Unk06);

            //    foreach (var emitter in system.Emitters)
            //    {

            //    }
            //}

            //// Finally, build header.
            //o.writeInt((int)Header.magic); // EFTF
            //o.writeInt(Header.unk0); // idk
            //o.writeInt(Systems.Count);
            //o.writeInt(Header.unk1);

            //offs = 0x50 + particlesBuf.Size;
            //o.writeInt(offs); // strings offset
            //o.writeInt(stringsBuf.Size);
            //offs += stringsBuf.Size;

            //o.writeInt(offs); // textures offset
            //o.writeInt(texturesBuf.Size);
            //offs += texturesBuf.Size;

            //o.writeInt(offs); // shaders offset
            //o.writeInt(shadersBuf.Size);
            //offs += shadersBuf.Size;

            //o.writeInt(offs); // keya offset
            //o.writeInt(keyaBuf.Size);
            //offs += keyaBuf.Size;

            //o.writeInt(offs); // unk1 offset
            //o.writeInt(UnkChunk1.Data.Length);
            //offs += UnkChunk1.Data.Length;

            //o.writeInt(offs); // unk2 offset
            //o.writeInt(UnkChunk2.Data.Length);

            //o.writeInt(Header.unk3);
            //o.writeInt(Header.unk4);
            //o.writeInt(Header.unk5);
            //o.writeInt(Header.unk6);
            //o.writeInt(Header.unk7);
            //o.writeInt(Header.unk8);

            //o.write(particlesBuf);
            //o.write(stringsBuf);
            //o.write(texturesBuf);
            //o.write(shadersBuf);
            //o.write(keyaBuf);
            //o.write(UnkChunk1.Data);
            //o.write(UnkChunk2.Data);

            //return o.GetBytes();
        }

        // hack. ignore
        //string Shell(string dir, string cmd, string args)
        //{
        //    var pProcess = new System.Diagnostics.Process();
        //    pProcess.StartInfo.FileName = cmd;
        //    pProcess.StartInfo.Arguments = args;
        //    pProcess.StartInfo.UseShellExecute = false;
        //    pProcess.StartInfo.RedirectStandardOutput = true;
        //    pProcess.StartInfo.WorkingDirectory = dir;

        //    pProcess.Start();
        //    string strOutput = pProcess.StandardOutput.ReadToEnd();
        //    pProcess.WaitForExit();

        //    return strOutput;
        //}
    }
}
