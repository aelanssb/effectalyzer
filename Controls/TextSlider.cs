﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Effectalyzer
{
    public enum TextSliderMode
    {
        Slider,
        Text
    }

    public class TextSlider : Label
    {
        // Designer properties
        [Category("Behavior"), Description("Minimum allowed value.")]
        public float MinimumValue { get; set; } = 0;
        [Category("Behavior"), Description("Maximum allowed value.")]
        public float MaximumValue { get; set; } = 1;
        [Category("Behavior"), Description("Amount added per pixel of mouse movement.")]
        public float Step { get; set; } = 0.001f;
        [Category("Appearance"), Description("Current value.")]
        public float Value
        {
            get { return value_; }

            set
            {
                value_ = Math.Max(MinimumValue, Math.Min(MaximumValue, value));
                Text = Value.ToString();
            }
        }

        public TextSliderMode Mode { get; set; } = TextSliderMode.Slider;

        float value_ = 0;
        bool GrabbingMouse = false;
        int LastX;
        TextBox EditControl = new TextBox();

        // Events
        [Category("Action"), Description("Occurs when the value of the control changes.")]
        public event EventHandler ValueChanged;

        public TextSlider() : base()
        {
            ForeColor = Color.Blue;
            Font = new Font(Font.FontFamily, Font.Size, FontStyle.Underline);
            Cursor = Cursors.SizeWE;
            DoubleBuffered = true;
            SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);

            Text = Value.ToString();

            MouseDown += TextSlider_MouseDown;
            MouseUp += TextSlider_MouseUp;
            MouseMove += TextSlider_MouseMove;
            DoubleClick += TextSlider_DoubleClick;
        }

        private void TextSlider_DoubleClick(object sender, EventArgs e)
        {
            if (Mode == TextSliderMode.Slider)
            {
                Controls.Add(EditControl);
                Mode = TextSliderMode.Text;
                EditControl.Text = Text;
                EditControl.Focus();
                EditControl.KeyDown += EditControl_KeyDown;
            }
        }

        private void EditControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Value = (float)new SingleConverter().ConvertFromString(EditControl.Text);

                Controls.Remove(EditControl);
                Mode = TextSliderMode.Slider;
                OnValueChanged(new EventArgs());
            }
        }

        protected virtual void OnValueChanged(EventArgs e)
        {
            //Text = Value.ToString();
            ValueChanged?.Invoke(this, e);
        }

        private void TextSlider_MouseDown(object sender, MouseEventArgs e)
        {
            GrabbingMouse = true;
            LastX = e.X;
        }

        private void TextSlider_MouseUp(object sender, MouseEventArgs e)
        {
            GrabbingMouse = false;
        }

        // TODO: Validation
        private void TextSlider_MouseMove(object sender, MouseEventArgs e)
        {
            if (!GrabbingMouse)
                return;

            Value += ((e.X - LastX) * Step);
            LastX = e.X;

            OnValueChanged(new EventArgs());
        }
    }
}
