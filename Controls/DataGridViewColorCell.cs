﻿using OpenTK;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Effectalyzer
{
    public class DataGridViewColorCell : DataGridViewTextBoxCell
    {
        public override void InitializeEditingControl(int rowIndex, object initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
        {
            base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);

            var ctl = (DataGridView.EditingControl as DataGridViewTextBoxEditingControl);
            ctl.ReadOnly = true;
            using (var cd = new ColorPickerDialog((Vector4)Value))
            {
                cd.Value = (Vector4)Value;

                if (cd.ShowDialog() == DialogResult.OK)
                    Value = cd.Value;
            }

            SetStyleColors();
        }

        public void SetStyleColors()
        {
            Style.BackColor = RGBA.ToColor((Vector4)Value);
            Style.ForeColor = Style.BackColor; // CalculateTextColor(Style.BackColor);
        }

        Color CalculateTextColor(Color backgroundColor)
        {
            int yiq = ((backgroundColor.R * 299) + (backgroundColor.G * 587) + (backgroundColor.B * 114));

            if (yiq >= 0x1F400)
                return Color.Black;
            else
                return Color.White;
        }

        public override Type EditType
        {
            get
            {
                return typeof(DataGridViewTextBoxEditingControl);
            }
        }

        public override Type ValueType
        {
            get
            {
                return typeof(Vector4);
            }
        }

        public override object DefaultNewRowValue
        {
            get
            {
                return Vector4.One;
            }
        }
    }
}
