﻿using System;
using System.Collections.Generic;

namespace Effectalyzer
{
    public class EFC
    {
        public class Effect
        {
            public uint Unk0;
            public int ParticleId;
            public int ModelNameId;
            public short Unk1;
            public short Unk2;
        }

        public class MeshInfo
        {
            public ushort Unk0;
            public ushort Unk1;

            public string Name;
        }

        public uint Unk0;
        public List<Effect> Effects = new List<Effect>();
        public List<MeshInfo> Meshes = new List<MeshInfo>();
        public List<string> ModelNames = new List<string>();
        public List<string> MeshNames = new List<string>();

        public EFC() { }

        public EFC (string filename)
        {
            InputBuffer file = new InputBuffer(filename);

            uint magic = (uint)file.readInt();
            Unk0 = (uint)file.readInt();
            short numEffects = file.readShort();
            short numModelNames = file.readShort();
            short numMeshes = file.readShort();
            file.skip(0x02); // pad

            for (int i = 0; i < numEffects; i++)
            {
                var effect = new Effect();
                effect.Unk0 = (uint)file.readInt();
                effect.ParticleId = file.readInt() - 1;
                effect.ModelNameId = file.readInt() - 1;
                effect.Unk1 = (short)(file.readShort() - 1);
                effect.Unk2 = (short)(file.readShort() - 1);

                Effects.Add(effect);
            }

            for (int i = 0; i < numMeshes; i++)
            {
                var info = new MeshInfo();
                info.Unk0 = (ushort)file.readShort();
                info.Unk1 = (ushort)file.readShort();
                Meshes.Add(info);
            }

            for (int i = 0; i < numModelNames; i++)
            {
                ModelNames.Add(file.readCString());
            }

            for (int i = 0; i < numMeshes; i++)
            {
                Meshes[i].Name = file.readCString();
                MeshNames.Add(Meshes[i].Name);
            }
        }
    }
}
